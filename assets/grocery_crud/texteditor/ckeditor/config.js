CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
 config.language = 'th';
 config.uiColor = '#AADC6E';

	 // added code for ckfinder ------>
	 config.filebrowserBrowseUrl = '/richy/assets/grocery_crud/texteditor/ckfinder/ckfinder.html';
	 config.filebrowserImageBrowseUrl = '/richy/assets/grocery_crud/texteditor/ckfinder/ckfinder.html?type=Images';
	 config.filebrowserFlashBrowseUrl = '/richy/assets/grocery_crud/texteditor/ckfinder/ckfinder.html?type=Flash';
	 config.filebrowserUploadUrl = '/richy/assets/grocery_crud/texteditor/ckfinder/core/connector /php/connector.php?command=QuickUpload&type=Files';
	 config.filebrowserImageUploadUrl = '/richy/assets/grocery_crud/texteditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
	 config.filebrowserFlashUploadUrl = '/richy/assets/grocery_crud/texteditor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
  config.contentsCss = '/richy/assets/css/main.css';
config.font_names = 'supermarket;' + config.font_names;
// end: code for ckfinder ------>


};
