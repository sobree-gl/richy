-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 26, 2019 at 12:18 PM
-- Server version: 5.7.23
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `richy`
--

-- --------------------------------------------------------

--
-- Table structure for table `board`
--

DROP TABLE IF EXISTS `board`;
CREATE TABLE IF NOT EXISTS `board` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `board`
--

INSERT INTO `board` (`id`, `title_th`, `title_en`, `title_ch`, `detail_th`, `detail_en`, `detail_ch`, `date`) VALUES
(2, 'คณะกรรมการบริษัท', 'Board of Directors', NULL, '<p>\n	<span style=\"color: rgb(10, 10, 10); font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 16px; background-color: rgb(254, 254, 254);\">มีจำนวน 11 ท่าน ประกอบด้วย</span></p>\n', '<p>\n	<span style=\"color: rgb(10, 10, 10); font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 16px; background-color: rgb(254, 254, 254);\">The Board of Directors, consisted of 11 members as follows:</span></p>', NULL, '2019-11-21 05:10:45'),
(3, 'คณะกรรมการตรวจสอบ', 'The Audit Committee', NULL, '<p>\n	<span style=\"color: rgb(10, 10, 10); font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 16px; background-color: rgb(254, 254, 254);\">คณะกรรมการตรวจสอบทุกท่าน เป็นผู้ที่มีความรู้ความสามารถด้านบัญชีและการเงิน มีจำนวน 3 ท่าน ประกอบด้วย</span></p>\n', '<p>\n	<span style=\"color: rgb(10, 10, 10); font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 16px; background-color: rgb(254, 254, 254);\">The Audit Committee, consisted of 3 members as follows:</span></p>\n', NULL, '2019-11-21 05:20:59');

-- --------------------------------------------------------

--
-- Table structure for table `board_list`
--

DROP TABLE IF EXISTS `board_list`;
CREATE TABLE IF NOT EXISTS `board_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `board_id` int(11) NOT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `position_th` varchar(255) DEFAULT NULL,
  `position_en` varchar(255) DEFAULT NULL,
  `position_ch` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `board_list`
--

INSERT INTO `board_list` (`id`, `board_id`, `title_th`, `title_en`, `title_ch`, `position_th`, `position_en`, `position_ch`, `date`) VALUES
(2, 2, 'นายพระนาย สุวรรณรัฐ', 'Mr.Pranai Suwanrath', NULL, 'ประธานกรรมการและกรรมการอิสระ', 'Chairman / Independent Director', NULL, '2019-11-21 05:12:36'),
(3, 2, 'ดร.อาภา อรรถบูรณ์วงศ์', 'Dr. Apa Attaboonwong', NULL, 'รองประธานกรรมการ', 'Vice Chairman', NULL, '2019-11-21 05:13:32'),
(4, 3, 'พลโทหญิงสำอางค์ ทองปาน', 'Lt. Gen. Samang Thongpan ', NULL, 'ประธานกรรมการตรวจสอบ / กรรมการอิสระ', 'Chairman of the Audit Committee / Independent Director', NULL, '2019-11-21 05:21:28');

-- --------------------------------------------------------

--
-- Table structure for table `calendar`
--

DROP TABLE IF EXISTS `calendar`;
CREATE TABLE IF NOT EXISTS `calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `place_th` varchar(255) DEFAULT NULL,
  `place_en` varchar(255) DEFAULT NULL,
  `place_ch` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `vdo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `calendar`
--

INSERT INTO `calendar` (`id`, `date`, `title_th`, `title_en`, `title_ch`, `place_th`, `place_en`, `place_ch`, `file`, `vdo`) VALUES
(1, '2019-11-13 03:12:18', 'บริษัทจดทะเบียนพบผู้ลงทุน สำหรับผลประกอบการไตรมาส 3/2561', 'Annual General Meeting of Shareholders 2019', NULL, 'ณ ห้องประชุม 603 อาคารตลาดหลักทรัพย์แห่งประเทศไทย 93 ถนนรัชดาภิเษก แขวง ดินแดง เขต ดินแดง กรุงเทพมหานคร', 'SD Avenue Hotel, 3rd Floor, Pin Klao room', NULL, '232b3-20181128140044.pdf', '4b8f4-movie.mp4');

-- --------------------------------------------------------

--
-- Table structure for table `chairman`
--

DROP TABLE IF EXISTS `chairman`;
CREATE TABLE IF NOT EXISTS `chairman` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `groups` enum('ประธานกรรมการบริษัท - Chairman of the Board','ประธานกรรมการบริหาร - Chief Executive Officer') NOT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chairman`
--

INSERT INTO `chairman` (`id`, `image`, `groups`, `title_th`, `title_en`, `title_ch`, `detail_th`, `detail_en`, `detail_ch`, `date`) VALUES
(1, 'c9b65-img_06.png', 'ประธานกรรมการบริษัท - Chairman of the Board', 'สารจากประธานกรรมการบริษัท', 'Message from Chairman of the Board', NULL, '<p style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(10, 10, 10); background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important;\">\n	เรียนท่านผู้ถือหุ้น</p>\n<p>\n	&nbsp;</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	นับตั้งแต่ก่อตั้งบริษัท ริ ชี่ เพลซ 2002 จำกัด (มหำชน) เป็นบริษัทพัฒนาอสังหาริมทรัพย์ตลอดระยะเวลา 17 ปี บริษัทยังคงมุ่นมั่นพัฒนา ทุ่มเทกาลังทั้งหมดที่มี เพื่อสรรสร้างบ้านในฝันที่ดี มีคุณภาพ ซึ่งทุกโครงการล้วนแต่ผ่านการวางแผนอย่างรอบคอบทุกขั้นตอนเพื่อสร้างและส่งมอบสินค้า การบริการที่ดีมีคุณภาพควบคู่ไปกับการดูแลสิ่งแวดล้อมและสังคมที่ยั่งยืน ถึงแม้ในปี 2561 ยอดการรับรู้รายได้จะไม่เป็นไปตามเป้าหมาย แต่เราก็ไม่เคยย่อท้อ ยังคงมุ่งมั่นที่จะดำเนินธุรกิจในปี 2562 ให้เป็นไปตามเป้าหมายของบริษัท กล่าวคือ ดำเนินการขยายธุรกิจให้มากขึ้น ทั้งด้านจำนวนโครงการและขนาดของโครงการ และยึดถือความต้องการของผู้อยู่อาศัยและ นักลงทุนเป็นหลัก</p>\n<p>\n	&nbsp;</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	ในปี 2561 ที่ผ่านมา บริษัทได้รายได้จากการขายอสังหาริมทรัพย์เท่ากับ ล้านบาท เพิ่มขึ้นจากปี 2560 เท่ากับ 1,366.58 หรือเพิ่มขึ้นร้อยละ103.97 ซึ่งเป็นผลมาจากโครงการริชพาร์ค@ทริปเปิ้ลสเตชั่น สร้างเสร็จโอนกรรมสิทธิ์และสามารถรับรู้ได้ในปี 2561</p>\n<p>\n	&nbsp;</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	สำหรับพัฒนาการอื่นๆในรอบปีที่ผ่านมาของบริษัทฯและบริษัทย่อย มีดังนี้</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	1.เพิ่มทุนจดทะเบียนจากเดิม 1,289,363,596 บาท เป็น 1,381,460,996 บาท โดยเป็นทุนจดทะเบียนชำระแล้ว จำนวน 1,044,859,438 บาท เพื่อใช้รองรับหุ้นปันผลและเพื่อใช้รองรับการปรับสิทธิใบสำคัญแสดงสิทธิ RICHY-W1 และRICHY-W2</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	2.เปิดตัวโครงการ&ldquo;เดอะริช พระรำมเก้า-ศรีนครินทร์ ทริปเปิลสเตชั่น&rdquo; เป็นคอนโดมิเนียมมิกซ์ยูส คอนโดมิเนียมสูง 32 ชั้น มูลค่าประมาณ 1,700 ล้านบาท</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	3.เพิ่มทุนจดทะเบียนบริษัทย่อย จากเดิมทุนจดทะเบียน 1,000,000 บาท เป็น 31,000,000 บาท</p>\n<p style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(10, 10, 10); background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important;\">\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	ทั้งนี้ในฐานะกรรมการของบริษัท ริชี่ เพลซ 2002 จำกัด (มหาชน) ผมใคร่ขอขอบคุณท่านผู้ถือหุ้น ลูกค้า สถาบันการเงินสื่อมวลชน พันธมิตรทางธุรกิจ และส่วนราชการที่เกี่ยวข้อง ที่ให้การช่วยเหลือ สนับสนุนบริษัทในทุก ๆ ด้านด้วยดีมาโดยตลอด และขอขอบคุณคณะกรรมการ ผู้บริหาร ตลอดจนพนักงานทุกฝ่ายที่ได้ร่วมมือร่วมใจกันทำงาน ซึ่งจะส่งผลให้เราประสบความสำเร็จในวันข้างหน้ายิ่ง ๆ ขึ้นไป</p>\n<p>\n	&nbsp;</p>\n<div class=\"sign-chairman\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px; padding: 0px; float: right; overflow: hidden; color: rgb(10, 10, 10); font-size: 16px; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important;\">\n	<p style=\"box-sizing: inherit; margin: 0px 0px 0.5rem; padding: 0px; font-size: inherit; line-height: 1.7; text-rendering: optimizelegibility; -webkit-tap-highlight-color: transparent !important;\">\n		ขอแสดงความนับถือ</p>\n	<img alt=\"\" src=\"https://www.irplus.in.th/Listed/RICHY/images/img_07.png\" style=\"box-sizing: inherit; border: 0px none; vertical-align: middle; display: inline-block; max-width: 100%; height: auto; -webkit-tap-highlight-color: transparent !important;\" width=\"160\" />\n	<p style=\"box-sizing: inherit; margin: 0px 0px 0.5rem; padding: 0px; font-size: inherit; line-height: 1.7; text-rendering: optimizelegibility; -webkit-tap-highlight-color: transparent !important;\">\n		นายพระนาย สุวรรณรัฐ<br style=\"box-sizing: inherit; -webkit-tap-highlight-color: transparent !important;\" />\n		ประธานกรรมการ</p>\n</div>\n<p>\n	&nbsp;</p>\n', '<p style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(10, 10, 10); background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important;\">\n	Dear all shareholders,</p>\n<p>\n	&nbsp;</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	Since its establishment, Richy Place 2002 Public Company Limited has been a real estate development company for 17 years. Dedicated to all the strength In order to create a good quality dream house in which all projects are carefully planned every step to create and deliver products Good quality service coupled with sustainable environmental and social care, even in 2018, revenue recognition will not meet the target. But we have never discouraged Still aiming to run the business in 2019 to be in line with the company&#39;s goals, that is, continue to expand the business. Both the number of projects and the size of the project And based on the needs of residents and investors</p>\n<p>\n	&nbsp;</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	In the year 2018, the company had revenue from real estate sales of 2,680.99 million baht, an increase from 2017, equal to 1,366.58 or 103.97 percent, which was the result of the Rich Park @ Triple Station project. Am Completed, transferred ownership and can be recognized in 2018</p>\n<p>\n	&nbsp;</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	For other developments in the past year of the Company and its subsidiaries are as follows:</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	1. Increase registered capital from the original 1,289,363,596 baht to 1,381,460,996 baht, which is a paid-up capital of 1,044,859,438 baht to support the stock dividend and to support the right adjustment of the warrants RICHY-W1 and RICHY-W2</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	2. Launching the project &quot;The Rich Rama 9 - Srinakarin Triple Station &quot;is a mix of condominiums. 32-storey condominium worth about 1,700 million baht</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	3. Increase the subsidiary&#39;s registered capital From the original registered capital of 1,000,000 baht to 31,000,000 baht</p>\n<p style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(10, 10, 10); background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important;\">\n	&nbsp;</p>\n<p>\n	&nbsp;</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	As a director of Richy Place 2002 Public Company Limited, I would like to thank the shareholders, financial institutions, media, business partners. And related government agencies Helping Has always supported the company in all aspects And would like to thank the Board of Directors, executives, and all employees for working together Which will result in us becoming more successful in the future</p>\n<p>\n	&nbsp;</p>\n<div class=\"sign-chairman\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px; padding: 0px; float: right; overflow: hidden; color: rgb(10, 10, 10); font-size: 16px; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important;\">\n	<p style=\"box-sizing: inherit; margin: 0px 0px 0.5rem; padding: 0px; font-size: inherit; line-height: 1.7; text-rendering: optimizelegibility; -webkit-tap-highlight-color: transparent !important;\">\n		Sincerely yours,</p>\n	<img alt=\"\" src=\"https://www.irplus.in.th/Listed/RICHY/images/img_07.png\" style=\"box-sizing: inherit; border: 0px none; vertical-align: middle; display: inline-block; max-width: 100%; height: auto; -webkit-tap-highlight-color: transparent !important;\" width=\"160\" />\n	<p style=\"box-sizing: inherit; margin: 0px 0px 0.5rem; padding: 0px; font-size: inherit; line-height: 1.7; text-rendering: optimizelegibility; -webkit-tap-highlight-color: transparent !important;\">\n		Mr.Pranai Suwanrath<br style=\"box-sizing: inherit; -webkit-tap-highlight-color: transparent !important;\" />\n		Chairman</p>\n</div>\n<p>\n	&nbsp;</p>\n', NULL, '2019-11-15 10:30:10');

-- --------------------------------------------------------

--
-- Table structure for table `complaints_category`
--

DROP TABLE IF EXISTS `complaints_category`;
CREATE TABLE IF NOT EXISTS `complaints_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `complaints_category`
--

INSERT INTO `complaints_category` (`id`, `title_th`, `title_en`, `title_ch`, `date`) VALUES
(7, 'พบการทุจริต และ/หรือ ข้อสงสัย ภายในหน่วยงานของบริษัท', 'Found the defective of the officer and/or employee.', NULL, NULL),
(8, 'พบการบกพร่องของเจ้าหน้าที่ และ/หรือ พนักงานบริษัท', 'Found fraud and/or concerns within the company.', NULL, NULL),
(9, 'พบการดำเนินงาน/กิจกรรม ที่ไม่โปร่งใสต่อผู้ถือหุ้น', 'Meet operations/ activities that are not transparent to shareholders.', NULL, NULL),
(10, 'แจ้งข้อร้องเรียนอื่นๆ', 'Other complaints.', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `complaints_save`
--

DROP TABLE IF EXISTS `complaints_save`;
CREATE TABLE IF NOT EXISTS `complaints_save` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catagory` varchar(255) DEFAULT NULL,
  `description` text,
  `file1` varchar(255) DEFAULT NULL,
  `file2` varchar(255) DEFAULT NULL,
  `file3` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `telphone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `other` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `complaints_save`
--

INSERT INTO `complaints_save` (`id`, `catagory`, `description`, `file1`, `file2`, `file3`, `name`, `telphone`, `email`, `other`, `date`) VALUES
(1, '0', '', '02d3c3c097e360eb6634bbd95a1285b0.pdf', NULL, 'e20c8bf2632b921df6c0d9e8119a1baf.JPG', '', '', '', '', '2019-11-26 05:29:37'),
(2, '2', 'air investment, or found guilty of Acts', '291a08ac8117b1ee39f90c1cab48b9d8.gif', '515f448f72ddaed64604e0ca7ad8508e.pdf', NULL, 'thinker test', '000000', 'test@ff.com', 'and .exe does not allow.', '2019-11-26 07:34:30');

-- --------------------------------------------------------

--
-- Table structure for table `corporate`
--

DROP TABLE IF EXISTS `corporate`;
CREATE TABLE IF NOT EXISTS `corporate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `corporate`
--

INSERT INTO `corporate` (`id`, `title_th`, `title_en`, `title_ch`, `date`) VALUES
(4, 'ข้อมูลทั่วไปและข้อมูลสำคัญอื่น', 'General Information and Other References', NULL, '2019-11-22 11:19:00'),
(5, 'ข้อมูลทั่วไปของบุคคลอ้างอิงอื่นๆ', 'Other References', NULL, '2019-11-22 11:20:13');

-- --------------------------------------------------------

--
-- Table structure for table `corporate_list`
--

DROP TABLE IF EXISTS `corporate_list`;
CREATE TABLE IF NOT EXISTS `corporate_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `corporate_id` int(11) NOT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `corporate_list`
--

INSERT INTO `corporate_list` (`id`, `corporate_id`, `title_th`, `title_en`, `title_ch`, `detail_th`, `detail_en`, `detail_ch`, `date`) VALUES
(4, 4, 'ชื่อบริษัทที่ออกหลักทรัพย์', 'Company Name', NULL, '<div>\n	บริษัท ริชี่ เพลซ 2002 จำกัด (มหาชน)</div>\n<div>\n	RICHY PLACE 2002 PUBLIC COMPANY LIMITED</div>\n', '<p>\n	<span background-color:=\"\" font-size:=\"\" helvetica=\"\" style=\"color: rgb(153, 153, 153); font-family: Prompt, \" white-space:=\"\">RICHY PLACE 2002 PUBLIC COMPANY LIMITED</span></p>\n', NULL, '2019-11-25 03:28:34'),
(5, 4, 'ลักษณะการประกอบธุรกิจ ', 'Nature of Business', NULL, '<p>\n	<span style=\"color: rgb(153, 153, 153); font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 15.5px; white-space: nowrap; background-color: rgb(244, 244, 244);\">ประกอบธุรกิจพัฒนาอสังหาริมทรัพย์</span></p>\n', '<p>\n	<span style=\"color: rgb(153, 153, 153); font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 15.5px; white-space: nowrap; background-color: rgb(244, 244, 244);\">Real Estate Development</span></p>\n', NULL, '2019-11-22 11:19:21');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `customerNumber` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`customerNumber`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dividend`
--

DROP TABLE IF EXISTS `dividend`;
CREATE TABLE IF NOT EXISTS `dividend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `BoD` date DEFAULT NULL,
  `XD` date DEFAULT NULL,
  `Dividend` date DEFAULT NULL,
  `Type` varchar(255) DEFAULT NULL,
  `perShare` varchar(255) DEFAULT NULL,
  `Performance_th` varchar(255) DEFAULT NULL,
  `Performance_en` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dividend`
--

INSERT INTO `dividend` (`id`, `BoD`, `XD`, `Dividend`, `Type`, `perShare`, `Performance_th`, `Performance_en`, `date`) VALUES
(4, '2019-11-12', '2019-11-13', '2019-11-21', 'Cash Dividend', '0.02514', '01/01/18-31/12/18', '01/01/18-31/12/18', '2019-11-26 12:14:37'),
(5, '2018-11-13', '2018-06-14', '2018-11-15', 'Stock Dividend', '7:1', '01/01/61-31/12/61', '01/01/61-31/12/61', '2019-11-26 12:14:43'),
(6, '2017-11-15', '2017-11-17', '2017-11-04', 'Cash Dividend', '0.0087', '01/01/60-31/12/60', '01/01/60-31/12/60', '2019-11-26 12:09:50');

-- --------------------------------------------------------

--
-- Table structure for table `dividend_info`
--

DROP TABLE IF EXISTS `dividend_info`;
CREATE TABLE IF NOT EXISTS `dividend_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dividend_info`
--

INSERT INTO `dividend_info` (`id`, `image`, `detail_th`, `detail_en`, `detail_ch`, `date`) VALUES
(2, '2219f-img_08.png', '<div>\n	&nbsp;</div>\n<div>\n	&nbsp;</div>\n<div>\n	บริษัทมีนโยบายจ่ายเงินปันผลในอัตราไม่ต่ำกว่าร้อยละ 40.00 ของกำไรสุทธิหลังหักภาษีเงินได้ นิติบุคคลสำหรับงบการเงินเฉพาะกิจการ และหลังหักเงินสำรองต่างๆ ทุกประเภทตามที่กฎหมายและบริษัทได้กำหนดไว้ และการจ่ายเงินปันผลนั้นไม่มีผลกระทบต่อการดำเนินงานตามปกติของบริษัทอย่างมีนัยสำคัญ</div>\n<div>\n	&nbsp;</div>\n<div>\n	<span style=\"color:#008000;\">ทั้งนี้การจ่ายเงินปันผล ดังกล่าวอาจมีการเปลี่ยนแปลงได้ ขึ้นอยู่กับผลการดำเนินงานและฐานะการเงินของบริษัท สภาพคล่องของบริษัท แผนการขยายธุรกิจ ความจำเป็นและความเหมาะสมอื่นใดในอนาคต</span></div>\n<div>\n	&nbsp;</div>\n<div>\n	และปัจจัยอื่นๆ ที่เกี่ยวข้องในการบริหารงานของบริษัทตามที่ คณะกรรมการบริษัท และ/หรือผู้ถือหุ้นของบริษัทเห็นสมควร และการดำเนินการดังกล่าวจะต้องก่อให้เกิดประโยชน์สูงสุด ต่อผู้ถือหุ้น</div>\n<div>\n	&nbsp;</div>\n', '<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	The Company has a dividend policy on paying out no less than 40.00% of its net income after deducting corporate tax based on the separate financial statements and all provisional reserves required by law and by the Company. The dividend payment is subject to any effect of normal business operation significantly.</p>\n<p>\n	&nbsp;</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" 19px=\"\" background-color:=\"\" class=\"text_08\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	However, such dividend payment is subject to change depending on the operation result and financial status of the Company, liquidity, business expansion plans, necessity, and suitability in the future,</p>\n<p>\n	&nbsp;</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	including other factors related to the Company&rsquo;s management for which the Board of Directors and/or shareholders deem appropriately. Such actions must maximize the most beneficial to shareholders.</p>\n', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `governance`
--

DROP TABLE IF EXISTS `governance`;
CREATE TABLE IF NOT EXISTS `governance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `governance`
--

INSERT INTO `governance` (`id`, `image`, `file`, `title_th`, `title_en`, `title_ch`, `detail_th`, `detail_en`, `detail_ch`, `date`) VALUES
(1, 'dc5f3-gimmick-governance.png', '42e01-richy_khaohoon_050819.pdf', 'เนื่องด้วยบริษัทมีนโยบายการปฏิบัติตาม', 'For the trans parency and beneficial', NULL, '<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" class=\"font-gray-smoke\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	ข้อพึงปฏิบัติที่ดี (Code of Best Practice) เพื่อเสริมสร้างความโปร่งใส และเป็นประโยชน์ต่อการดำเนินธุรกิจของบริษัท อันจะทำให้เกิดความเชื่อมั่นในกลุ่มผู้ถือหุ้น ผู้ลงทุน และผู้เกี่ยวข้องทุกฝ่าย คณะกรรมการบริษัทจึงได้มีการกำหนดหลักการการกำกับดูแลกิจการ (Corporate Governance Policy) อ้างอิงจากหลักการการกำกับดุแลกิจการที่ดีสำหรับบริษัทจดทะเบียนตามแนวทางที่ตลาดหลักทรัพย์แห่งประเทศไทยกำหนด ซึ่งครอบคลุมโครงสร้างการกำกับดูแล แนวปฎิบัติในการกำกับดูแลกิจการ โครงสร้างการปฏิบัติงาน และนโยบายการปฏิบัติงาน เพื่อให้บริษัทปฏิบัติตามนโยบายการกำกับดูแลกิจการอย่างเคร่งครัด</p>\n<p>\n	&nbsp;</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" class=\"font-gray-smoke\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	บริษัทได้จัดทำแนวปฏิบัติในการกำกับดูแลกิจการ (Corporate Governance Policy) เพื่อเป็นแนวทางให้แก่คณะกรรมการ ผู้บริหารและพนักงานในการปฏิบัติตามกฏหมายกฏระเบียบและข้อบังคับที่เกี่ยวข้อง</p>\n<p>\n	&nbsp;</p>\n<h4 -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" class=\"font-green-forest\" color:=\"\" font-size:=\"\" font-weight:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	เพื่อให้มั่นใจว่าบริษัทได้ดำเนินธุรกิจด้วยความรับผิดชอบสูงสุด มีความโปร่งใส และปฏิบัติต่อผู้มีส่วนได้ส่วนเสียอย่างเท่าเทียมกัน</h4>\n<p>\n	&nbsp;</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" class=\"font-gray-smoke\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	รวมทั้งเพื่อสนับสนุนให้บริษัทมีการบริหารการจัดการด้วยความซื่อสัตย์สุจริต มีประสิทธิภาพและมีประสิทธิผล การกำกับดูแลกิจการที่บริษัทยึดเป็นหลักในการดำเนินธุรกิจมาอย่างต่อเนื่อง แบ่งเป็น 5 หมวด ได้แก่ สิทธิของผู้ถือหุ้น การปฏิบัติต่อผู้ถือหุ้นอย่างเท่าเทียมกัน การคำนึงถึงบทบาทของผู้มีส่วนได้เสีย การเปิดเผยข้อมูลและความโปร่งใส ความรับผิดชอบของคณะกรรมการ (รายละเอียดของแนวปฏิบัติในการกำกับดูแลกิจการของบริษัทสามารถดูได้จากเว็บไซต์ของบริษัท&nbsp;<a href=\"https://www.richy.co.th/\" style=\"box-sizing: inherit; background-color: transparent; line-height: inherit; color: rgb(13, 61, 33); text-decoration-line: none; cursor: pointer; -webkit-tap-highlight-color: transparent !important;\">(www.richy.co.th)</a>&nbsp;นอกจากนี้บริษัทยังจัดให้มีระเบียบปฏิบัติและคู่มือการปฏิบัติงาน เพื่อให้คณะผู้บริหารระดับสูงและพนักงานถือปฏิบัติในด้านต่างๆ</p>\n', '<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" class=\"font-gray-smoke\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	Contribution to the Company&rsquo;s business operation, the Company has policy to conduct in compliance with the Code of Best Practice in order to enhance the confidence to shareholders, investors, and all persons concerned. The Board of Directors has determined the corporate governance policy referred on the good governance practices for the listed companies of the Stock Exchanges of Thailand. This covers the good governance structure, guidelines of corporate governance practices, working structures, and policies in order to rigorously conduct under the corporate governance policy.</p>\n<p>\n	&nbsp;</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" class=\"font-gray-smoke\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	The Company has defined the code of corporate governance conduct to be the guidelines circulated to the directors, executives, and employees to conduct under the law, regulations, and related rules</p>\n<p>\n	&nbsp;</p>\n<h4 -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" class=\"font-green-forest\" color:=\"\" font-size:=\"\" font-weight:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	To ensure that the Company has carried on business with integrity, efficiency and effectiveness.</h4>\n<p>\n	&nbsp;</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" class=\"font-gray-smoke\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	The corporate governance of the Company that has been constantly conducted is comprised of the 5 items namely the rights of shareholders, equitable treatment of shareholders, roles of stakeholders, disclosure of information and transparency, and responsibility of the Board of Directors (details of the conduct of corporate governance of the Company are viewed on the Company&rsquo;s website&nbsp;<a href=\"https://www.richy.co.th/\" style=\"box-sizing: inherit; background-color: transparent; line-height: inherit; color: rgb(13, 61, 33); text-decoration-line: none; cursor: pointer; -webkit-tap-highlight-color: transparent !important;\">(www.richy.co.th)</a>. Moreover, the Company has provided other regulations and operation manuals for the high level executives and employee to implement.</p>\n', NULL, '2019-11-18 11:54:27');

-- --------------------------------------------------------

--
-- Table structure for table `ir_contact`
--

DROP TABLE IF EXISTS `ir_contact`;
CREATE TABLE IF NOT EXISTS `ir_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `map` varchar(1000) DEFAULT NULL,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ir_contact`
--

INSERT INTO `ir_contact` (`id`, `image`, `map`, `detail_th`, `detail_en`, `detail_ch`, `date`) VALUES
(1, '8aaea-logo-1-.png', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d124005.2404058901!2d100.477612!3d13.768996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe7c18ace45376964!2z4Lia4Lij4Li04Lip4Lix4LiXIOC4o-C4tOC4iuC4teC5iCDguYDguJ7guKXguIsgMjAwMiDguIjguLPguIHguLHguJQgKOC4oeC4q-C4suC4iuC4mSk!5e0!3m2!1sth!2sth!4v1574310634347!5m2!1sth!2sth\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>', '<p -webkit-tap-highlight-color:=\"\" 0px=\"\" 20px=\"\" background-color:=\"\" class=\"text_09\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-decoration-line:=\"\" text-rendering:=\"\" transparent=\"\" underline=\"\">\n	ผู้จัดการส่วนงานผู้ลงทุนสัมพันธ์</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	นางศรัณย์ธร ศรีสุนทร</p>\n<p>\n	&nbsp;</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" 20px=\"\" background-color:=\"\" class=\"text_09\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-decoration-line:=\"\" text-rendering:=\"\" transparent=\"\" underline=\"\">\n	สำนักงาน-ข้อมูลติดต่อ</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	บริษัท ริชี่ เพลซ 2002 จำกัด (มหาชน)<br style=\"box-sizing: inherit; -webkit-tap-highlight-color: transparent !important;\" />\n	เลขที่ 667/15 อาคารอรรถบูรณ์ ชั้น 7 เขตบางกอกน้อย ถนนจรัญสนิทวงศ์ แขวงอรุณอมรินทร์ กรุงเทพมหานคร 10700</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	โทรศัพท์ : 02-886-1817<br style=\"box-sizing: inherit; -webkit-tap-highlight-color: transparent !important;\" />\n	โทรสาร : 02-886-1060</p>\n', '<p -webkit-tap-highlight-color:=\"\" 0px=\"\" 20px=\"\" background-color:=\"\" class=\"text_09\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-decoration-line:=\"\" text-rendering:=\"\" transparent=\"\" underline=\"\">\n	Manager IR</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	Mrs.Saranthorn Srisunthorn</p>\n<p>\n	&nbsp;</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" 20px=\"\" background-color:=\"\" class=\"text_09\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-decoration-line:=\"\" text-rendering:=\"\" transparent=\"\" underline=\"\">\n	Office - Contact</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	RICHY PLACE 2002 PUBLIC COMPANY LIMITED<br style=\"box-sizing: inherit; -webkit-tap-highlight-color: transparent !important;\" />\n	667/15 Attaboon Building, 7th Floor Bangkok 10700 Thailand</p>\n<p -webkit-tap-highlight-color:=\"\" 0px=\"\" background-color:=\"\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" margin:=\"\" padding:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" text-rendering:=\"\" transparent=\"\">\n	Telaphone : 02-886-1817<br style=\"box-sizing: inherit; -webkit-tap-highlight-color: transparent !important;\" />\n	Fax : 02-886-1060</p>\n', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `major`
--

DROP TABLE IF EXISTS `major`;
CREATE TABLE IF NOT EXISTS `major` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `proportion` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `major`
--

INSERT INTO `major` (`id`, `title_th`, `title_en`, `title_ch`, `total`, `proportion`, `date`) VALUES
(4, 'น.ส.อาภา อรรถบูรณ์วงศ์', 'น.ส.อาภา อรรถบูรณ์วงศ์', NULL, '376,756,178.00', '36.06', '2019-11-21 11:17:37'),
(5, 'นายชัยสิทธิ์ วิริยะเมตตากุล', 'นายชัยสิทธิ์ วิริยะเมตตากุล', NULL, '98,150,000.00', '9.39', '2019-11-21 11:17:51');

-- --------------------------------------------------------

--
-- Table structure for table `major_info`
--

DROP TABLE IF EXISTS `major_info`;
CREATE TABLE IF NOT EXISTS `major_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `major_info`
--

INSERT INTO `major_info` (`id`, `detail_th`, `detail_en`, `detail_ch`, `date`) VALUES
(4, '<p style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(10, 10, 10); background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important;\">\n	หมายเหตุ :</p>\n<div style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px; padding: 0px; color: rgb(10, 10, 10); font-size: 16px; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important;\">\n	<span class=\"bull-sup\" style=\"box-sizing: inherit; top: -0.5em; background: rgb(8, 100, 67); color: rgb(255, 255, 255); padding: 0px 6px; border-radius: 50%; font-size: 11.84px; margin: 0px 5px 0px 0px; -webkit-tap-highlight-color: transparent !important;\">1</span>\n	<p style=\"box-sizing: inherit; margin: 0px 0px 0.5rem; padding: 0px; font-size: inherit; line-height: 1.7; text-rendering: optimizelegibility; -webkit-tap-highlight-color: transparent !important;\">\n		คือ บริษัทอรรถบูรณ์สินทรัพย์ จำกัด ประกอบธุรกิจอาคารสำนักงานให้เช่า โดย ณ วันที่ 30 เมษายน 2556 มีผู้ถือหุ้นประกอบด้วย น.ส.นงลักษณ์ วนธรรมพงศ์ ร้อยละ 20.87 นายพิชัย อรรถบูรณ์วงศ์ ร้อยละ 20.00 น.ส.อาภา อรรถบูรณ์วงศ์ ร้อยละ 19.10 นายปิติพัฒน์ พรพรหมพัฒน์ ร้อยละ 18.37 น.ส.พิชญา ตันโสด ร้อยละ 9.88 น.ส.สาธิณี อรรถบูรณ์วงศ์ ร้อยละ 5.86 นายสมศักดิ์ อรรถบูรณ์วงศ์ ร้อยละ 4.87 ด.ช.ณัฐภัทร อรรถบูรณ์วงศ์ ร้อยละ 0.52 ด.ช.ศุภณัฐ อรรถบูรณ์วงศ์ ร้อยละ 0.52</p>\n</div>\n<p>\n	&nbsp;</p>\n', '<p style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(10, 10, 10); background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important;\">\n	หมายเหตุ :</p>\n<div style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px; padding: 0px; color: rgb(10, 10, 10); font-size: 16px; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important;\">\n	<span class=\"bull-sup\" style=\"box-sizing: inherit; top: -0.5em; background: rgb(8, 100, 67); color: rgb(255, 255, 255); padding: 0px 6px; border-radius: 50%; font-size: 11.84px; margin: 0px 5px 0px 0px; -webkit-tap-highlight-color: transparent !important;\">1</span>\n	<p style=\"box-sizing: inherit; margin: 0px 0px 0.5rem; padding: 0px; font-size: inherit; line-height: 1.7; text-rendering: optimizelegibility; -webkit-tap-highlight-color: transparent !important;\">\n		คือ บริษัทอรรถบูรณ์สินทรัพย์ จำกัด ประกอบธุรกิจอาคารสำนักงานให้เช่า โดย ณ วันที่ 30 เมษายน 2556 มีผู้ถือหุ้นประกอบด้วย น.ส.นงลักษณ์ วนธรรมพงศ์ ร้อยละ 20.87 นายพิชัย อรรถบูรณ์วงศ์ ร้อยละ 20.00 น.ส.อาภา อรรถบูรณ์วงศ์ ร้อยละ 19.10 นายปิติพัฒน์ พรพรหมพัฒน์ ร้อยละ 18.37 น.ส.พิชญา ตันโสด ร้อยละ 9.88 น.ส.สาธิณี อรรถบูรณ์วงศ์ ร้อยละ 5.86 นายสมศักดิ์ อรรถบูรณ์วงศ์ ร้อยละ 4.87 ด.ช.ณัฐภัทร อรรถบูรณ์วงศ์ ร้อยละ 0.52 ด.ช.ศุภณัฐ อรรถบูรณ์วงศ์ ร้อยละ 0.52</p>\n</div>\n<p>\n	&nbsp;</p>\n', NULL, '2019-11-21 11:11:19');

-- --------------------------------------------------------

--
-- Table structure for table `meeting`
--

DROP TABLE IF EXISTS `meeting`;
CREATE TABLE IF NOT EXISTS `meeting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subcat_id` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meeting`
--

INSERT INTO `meeting` (`id`, `subcat_id`, `file`, `title_th`, `title_en`, `title_ch`, `date`) VALUES
(1, 4, NULL, 'Lorem ipsum dolor sit amet,', 'Lorem ipsum dolor sit amet,', NULL, '2019-11-21 05:56:59'),
(2, 3, '9f168-20181128140044.pdf', 'Lorem ipsum dolor sit amet,', 'Lorem ipsum dolor sit amet,', NULL, '2019-11-21 07:36:52');

-- --------------------------------------------------------

--
-- Table structure for table `meeting_category`
--

DROP TABLE IF EXISTS `meeting_category`;
CREATE TABLE IF NOT EXISTS `meeting_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meeting_category`
--

INSERT INTO `meeting_category` (`id`, `title_th`, `title_en`, `title_ch`, `date`) VALUES
(1, 'Lorem ipsum dolor sit amet,', NULL, NULL, '2019-11-08 08:15:39'),
(2, 'Lorem ipsum dolor sit amet,2', NULL, NULL, '2019-11-08 10:25:19');

-- --------------------------------------------------------

--
-- Table structure for table `meeting_subcategory`
--

DROP TABLE IF EXISTS `meeting_subcategory`;
CREATE TABLE IF NOT EXISTS `meeting_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `meeting_subcategory`
--

INSERT INTO `meeting_subcategory` (`id`, `cat_id`, `title_th`, `title_en`, `title_ch`, `date`) VALUES
(2, 2, 'Lorem ipsum dolor sit amet,', NULL, NULL, '2019-11-11 05:24:23'),
(3, 2, 'Lorem ipsum dolor sit amet,', NULL, NULL, '2019-11-11 05:31:54'),
(4, 1, 'Lorem ipsum dolor sit amet,', 'Lorem ipsum dolor sit amet,', NULL, '2019-11-21 05:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `news_clipping`
--

DROP TABLE IF EXISTS `news_clipping`;
CREATE TABLE IF NOT EXISTS `news_clipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news_clipping`
--

INSERT INTO `news_clipping` (`id`, `file`, `image`, `title_th`, `title_en`, `title_ch`, `date`) VALUES
(2, '1046d-20181128140044.pdf', '4e997-kaohoon.gif', 'ข่าวย่อย: RICHYร่วมงานมหกรรมบ้านฯ', 'ข่าวย่อย: RICHYร่วมงานมหกรรมบ้านฯ', NULL, '2019-11-20');

-- --------------------------------------------------------

--
-- Table structure for table `news_update`
--

DROP TABLE IF EXISTS `news_update`;
CREATE TABLE IF NOT EXISTS `news_update` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news_update`
--

INSERT INTO `news_update` (`id`, `title_th`, `title_en`, `title_ch`, `detail_th`, `detail_en`, `detail_ch`, `date`) VALUES
(2, 'RICHY ชูเรือธงปลายปี “ริชพาร์ค เทอมินอล พหลโยธิน 59”', NULL, NULL, '<table align=\"center\" border=\"0\" cellpadding=\"10\" style=\"font-family: Tahoma;\" width=\"90%\">\n	<tbody>\n		<tr>\n			<td colspan=\"2\" style=\"font-size: 13px; color: rgb(102, 102, 102); margin-left: 0px; margin-top: 0px; margin-bottom: 0px;\" valign=\"top\">\n				<table border=\"0\" valign=\"top\" width=\"100%\">\n					<tbody>\n						<tr>\n							<td style=\"margin-left: 0px; margin-top: 0px; margin-bottom: 0px;\" width=\"40\">\n								<img alt=\"www.irplus.in.th\" height=\"37\" src=\"https://www.efinancethai.com/news/images/logo_irplus.jpg\" style=\"cursor: pointer;\" width=\"39\" /></td>\n							<td style=\"margin-left: 0px; margin-top: 0px; margin-bottom: 0px;\" valign=\"middle\">\n								<font face=\"ms sans serif\" size=\"3\"><strong>Investor Relations Info&nbsp;:&nbsp;<a href=\"https://www.efinancethai.com/news/linkirplus.aspx?type=1&amp;listed=RICHY\" style=\"color: rgb(102, 102, 102); text-decoration-line: none;\" target=\"_Blank\">RICHY</a></strong></font></td>\n						</tr>\n					</tbody>\n				</table>\n			</td>\n		</tr>\n		<tr>\n			<td colspan=\"2\" style=\"font-size: 13px; color: rgb(102, 102, 102); margin-left: 0px; margin-top: 0px; margin-bottom: 0px;\" valign=\"top\">\n				<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" valign=\"top\">\n					<tbody>\n						<tr>\n							<td style=\"margin-left: 0px; margin-top: 0px; margin-bottom: 0px;\" valign=\"top\">\n								<font face=\"ms sans serif\" size=\"3\"><b>RICHY ชูเรือธงปลายปี &ldquo;ริชพาร์ค เทอมินอล พหลโยธิน 59&rdquo;</b></font></td>\n						</tr>\n						<tr>\n							<td align=\"center\" style=\"margin-left: 0px; margin-top: 0px; margin-bottom: 0px;\" valign=\"top\">\n								<img border=\"2\" hspace=\"5\" src=\"https://www.efinancethai.com/news/picture/2019/8/20/T/5159054.JPG\" vspace=\"5\" /></td>\n						</tr>\n						<tr>\n							<td style=\"margin-left: 0px; margin-top: 0px; margin-bottom: 0px;\" valign=\"top\">\n								<br />\n								<font face=\"ms sans serif\" size=\"3\"><b>สำนักข่าวอีไฟแนนซ์ไทย- -20 ส.ค. 62 12:36 น.</b></font><br />\n								<pre>\n</pre>\n								<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"590\">\n									<tbody>\n										<tr>\n											<td style=\"margin-left: 0px; margin-top: 0px; margin-bottom: 0px;\">\n												<p>\n													<br />\n													<font face=\"ms sans serif\" size=\"3\"><font face=\"ms sans serif\" size=\"3\">&emsp;&emsp;บมจ.ริชี่ เพลซ 2002 (RICHY) ประกาศชูเรือธง &ldquo;ริชพาร์ค เทอมินอล พหลโยธิน 59&rdquo; คอนโดมิเนียม เพดานสูง สไตล์ Loft&nbsp; มูลค่าโครงการกว่า 1.5 พันล้านบาท ชูจุดเด่นติดแนวรถไฟฟ้า BTS สถานีพหลโยธิน 59 แค่ 0 เมตร เชื่อมต่อด้วย Sky Walk พร้อมความสะดวกสบายครบครัน และเพลิดเพลินไปกับ Shopping Mall ภายในโครงการ ด้านผู้บริหาร &ldquo;ดร.อาภา อรรถบูรณ์วงศ์&rdquo;&nbsp; มั่นใจได้รับการตอบรับจากอย่างดีเยี่ยม แถมมีปัจจัยบวกจากแนวโน้มดอกเบี้ยขาลงเป็นแรงหนุน พร้อมเก็บเกี่ยวรายได้โค้งสุดท้ายปลายปี ปิดยอดดันยอดขายทั้งปี 3,600 ล้านบาท</font></font></p>\n												<p>\n													<font face=\"ms sans serif\" size=\"3\"><font face=\"ms sans serif\" size=\"3\">&emsp;&emsp;ดร.อาภา อรรถบูรณ์วงศ์ ประธานกรรมการบริหาร บริษัท ริชี่ เพลซ 2002 จำกัด (มหาชน) (RICHY) เปิดเผยว่า บริษัทฯเตรียมเปิดตัว ริช พาร์ค เทอมินอล พหลโยธิน 59 (Rich Park Terminal @ Phahonyothin 59) โครงการคอนโดมิเนียม เพดานสูง สไตล์ Loft ติดแนวรถไฟฟ้าสายสีเขียว หมอชิต-สะพานใหม่-คูคต มูลค่าโครงการ 1,500 ล้านบาท ซึ่งถือเป็นการเปิดมิติใหม่สำหรับโครงการคอนโดฯ ภายใต้การบริหารของ RICHY และมั่นใจว่าจะได้รับการตอบรับจากผู้บริโภคอย่างดีเยี่ยมจากผู้บริโภค เนื่องจากตั้งอยู่ในทำเลที่มีศักยภาพ ห่างจากสถานีรถไฟฟ้าพหลโยธิน 59 แค่ 0 เมตร อยู่บริเวณใกล้โลตัส หลักสี่, บิ๊กซี สะพานใหม่ เซ็นทรัล รามอินทรา, ตลาดยิ่งเจริญ, ม.เกษตรศาสตร์ และ ม.ศรีปทุม</font></font></p>\n												<p>\n													<font face=\"ms sans serif\" size=\"3\"><font face=\"ms sans serif\" size=\"3\">&emsp;&emsp;ภายใต้จุดเด่นที่สำคัญคือ คอนโดฯ เชื่อมต่อสถานีรถไฟฟ้าทันทีด้วย Sky Walk พร้อมสิ่งอำนวยความสะดวกครบครัน โดยผู้อยู่อาศัยยังสามารถเพลิดเพลินกับ Shopping Mall ภายในโครงการ นอกจากนี้ ยังมี Jogging Track โอบล้อม สวนสวย ชั้น 15 Fitness, Swimming Pool ระบบเกลือพร้อมด้วย Lobby Co-Working Space ตอบโจทย์ Lifestyle คนรุ่นใหม่ได้อย่างลงตัว</font></font></p>\n												<p>\n													<font face=\"ms sans serif\" size=\"3\"><font face=\"ms sans serif\" size=\"3\">&emsp;&emsp;ทั้งนี้ ริชพาร์ค เทอมินอล หลักสี่ เป็นคอนโด High Rise 14 ชั้น 1 อาคาร พื้นที่โครงการขนาด 3-1-05 ไร่ ห้องพักอาศัยรวม 563 ยูนิต มีห้องพักให้เลือกแบบ 1 Bedroom และ 1 Bedroom Plus ขนาดเริ่มต้น 28.00-34.70 ตร.ม. คาดว่าแล้วเสร็จพร้อมเข้าอยู่ปี 2562 ราคาเริ่มต้นที่ 2.29 ล้านบาท</font></font></p>\n												<p>\n													<font face=\"ms sans serif\" size=\"3\"><font face=\"ms sans serif\" size=\"3\">&emsp;&emsp;ประธานกรรมการบริหาร RICHY กล่าวอีกว่า แนวโน้มดอกเบี้ยที่อยู่ในช่วงขาลงในขณะนี้ หลังจากธนาคารพาณิชย์ขนาดใหญ่ปรับลดอัตราดอกเบี้ยเงินกู้ลง 0.25% ตามดอกเบี้ยนโยบายของธนาคารแห่งประเทศไทย (ธปท.) เชื่อว่าจะมีส่วนสำคัญในการกระตุ้นกำลังซื้อ และทำให้ภาพรวมตลาดอสังหาริมทรัพย์ฟื้นตัว โดยบริษัทฯจะเริ่มทยอยรับรู้รายได้จากโครงการ &ldquo;ริชพาร์ค เทอมินอล หลักสี่&rdquo; ในช่วงไตรมาส 4/62</font></font></p>\n												<p>\n													<font face=\"ms sans serif\" size=\"3\"><font face=\"ms sans serif\" size=\"3\">&nbsp;</font></font></p>\n											</td>\n										</tr>\n									</tbody>\n								</table>\n							</td>\n						</tr>\n					</tbody>\n				</table>\n			</td>\n		</tr>\n		<tr>\n			<td colspan=\"2\" style=\"font-size: 13px; color: rgb(102, 102, 102); margin-left: 0px; margin-top: 0px; margin-bottom: 0px;\" valign=\"top\">\n				<table align=\"center\" border=\"0\" width=\"100%\">\n					<tbody>\n						<tr>\n							<td style=\"margin-left: 0px; margin-top: 0px; margin-bottom: 0px;\" valign=\"top\">\n								<pre>\n<u>เรียบเรียง</u>&nbsp;โดย&nbsp;ชุติมา มุสิกะเจริญ&nbsp; \n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;อีเมล์. reporter@efinancethai.com\n</pre>\n							</td>\n						</tr>\n					</tbody>\n				</table>\n			</td>\n		</tr>\n	</tbody>\n</table>\n<p>\n	<br style=\"color: rgb(102, 102, 102); font-family: Tahoma; font-size: 13px; text-align: -webkit-center;\" />\n	&nbsp;</p>\n<center style=\"color: rgb(102, 102, 102); font-family: Tahoma; font-size: 13px;\">\n	<p>\n		<input class=\"button_style\" name=\"bclose\" style=\"background-color: rgb(207, 207, 207); height: 30px; border-style: solid; border-color: rgb(192, 192, 192); padding: 5px; width: 80px; cursor: pointer;\" tabindex=\"1\" type=\"submit\" value=\" Close \" /></p>\n</center>\n', NULL, NULL, '2019-11-12');

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

DROP TABLE IF EXISTS `organization`;
CREATE TABLE IF NOT EXISTS `organization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`id`, `image`, `date`) VALUES
(1, '91e14-chart_edit_010219_en.jpg', '2019-11-18 10:31:38');

-- --------------------------------------------------------

--
-- Table structure for table `passion`
--

DROP TABLE IF EXISTS `passion`;
CREATE TABLE IF NOT EXISTS `passion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `passion`
--

INSERT INTO `passion` (`id`, `image`, `title_th`, `title_en`, `title_ch`, `detail_th`, `detail_en`, `detail_ch`, `date`) VALUES
(1, '6a1d3-capture.jpg', 'วิสัยทัศน์', 'Vision', NULL, '<p style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(10, 10, 10); text-align: right; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important;\">\n	เป็นบริษัทชั้นนำในด้านการพัฒนาอสังหาริม ทรัพย์ในประเทศไทย โดยมุ่งเน้นความต้องการ ความพึงพอใจ และความสุขในการอยู่อาศัย ของลูกค้า ทุกกลุ่มเป้าหมายที่ประกอบด้วย รูปแบบผลิตภัณฑ์ ทำเลที่ตั้งโครงการ ความ เป็นมิตรกับสิ่งแวดล้อม และการบริการที่มีประ สิทธิภาพในระดับแนวหน้า</p>\n<p class=\"text_08\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 1.7; text-rendering: optimizelegibility; text-align: right; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; margin-bottom: 25px !important; font-size: 19px !important; color: rgb(0, 128, 74) !important;\">\n	เพื่อสร้างความเชื่อ ถือในระยะยาวจากผู้ที่เกี่ยวข้องทั้งปวง</p>\n', '<p style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(10, 10, 10); text-align: right; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important;\">\n	he Company is prominent in real estate development sector emphasizing the need, satisfaction, and happiness in living to the target consumers, which is the product pattern, site location, environment friendliness, outstanding efficient service</p>\n<p class=\"text_08\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin-top: 0px; margin-right: 0px; margin-left: 0px; padding: 0px; line-height: 1.7; text-rendering: optimizelegibility; text-align: right; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; margin-bottom: 25px !important; font-size: 19px !important; color: rgb(0, 128, 74) !important;\">\n	to gain confidence to all parties concerned.</p>\n', NULL, '2019-11-18 10:40:48'),
(2, '7b587-capture.jpg', 'พันธกิจ', 'Mission', NULL, '<ul -webkit-tap-highlight-color:=\"\" 0.5rem=\"\" 0px=\"\" background-color:=\"\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" list-style:=\"\" margin:=\"\" padding-bottom:=\"\" padding-left:=\"\" padding-right:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" transparent=\"\">\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		เป็นบริษัท พัฒนาอสังหาริมทรัพย์ชั้นนำ 1 ใน 20 ของประเทศไทยภายใน 10 ปี</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		สร้างผลตอบแทนการลงทุนแก่ผู้ถือหุ้นในอัตราไม่น้อยกว่าร้อยละ 10</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		ให้ผลตอบแทนการทำงานแก่พนักงานสูงกว่าเฉลี่ยของอุตสาหกรรมภายใน 3 ปี</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		บุคลากรมีขีดความสามารถสูงเทียบเท่าบริษัทชั้นนำใน 3 ปี</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		สร้างวัฒนธรรมองค์กรที่ฉลาด ขยันและทุ่มเท</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		สร้างเครือข่ายพันธมิตรทางธุรกรรมการพัฒนาอสังหาริมทรัพย์</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		มีบริการหลังการขายที่รวดเร็วและสร้างความพึงพอใจให้ลูกค้า</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		สร้างคุณภาพชีวิตที่ดีต่อสังคม ชุมชนและสิ่งแวดล้อม</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		การบริหารต้นทุนที่แข่งขันได้ในอุตสาหกรรมภายใน 3 ปี</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		สร้างภาพลักษณ์องค์กรให้เป็นที่รู้จักและยอมรับภายใน 3 ปี</li>\n</ul>\n', '<ul -webkit-tap-highlight-color:=\"\" 0.5rem=\"\" 0px=\"\" background-color:=\"\" color:=\"\" font-size:=\"\" helvetica=\"\" line-height:=\"\" list-style:=\"\" margin:=\"\" padding-bottom:=\"\" padding-left:=\"\" padding-right:=\"\" style=\"box-sizing: inherit; font-family: Prompt, \" transparent=\"\">\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		One of top 20 real estate developing companies in Thailand within 10 years</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		10% yield upon investment to shareholders</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		Bonus to employees higher than average for the industry within 3 years</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		Personnel efficiency compatible to leading companies within 3 years</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		Create corporate culture to be smart, diligent, and dedicate.</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		Build business allies in real estate development</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		Prompt after sale service to satisfy consumers</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		Make better living to the society, community, and environment</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		Manage capital to be able to compete in the industry within 3 years</li>\n	<li style=\"box-sizing: inherit; margin: 0px; padding: 0px; font-size: inherit; -webkit-tap-highlight-color: transparent !important;\">\n		Build corporate image to be known and accepted within 3 years</li>\n</ul>\n', NULL, '2019-11-18 11:10:16');

-- --------------------------------------------------------

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
CREATE TABLE IF NOT EXISTS `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile`
--

INSERT INTO `profile` (`id`, `title_th`, `title_en`, `title_ch`, `detail_th`, `detail_en`, `detail_ch`, `date`) VALUES
(1, 'ปีพุทธศักราช : 2545', 'Year : 2002', NULL, '<p class=\"text_10\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; line-height: 1.7; text-rendering: optimizelegibility; -webkit-tap-highlight-color: transparent !important; font-size: 14px !important; color: rgb(8, 100, 67) !important;\">\n	เดือนพฤศจิกายน</p>\n<p style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(123, 126, 133); -webkit-tap-highlight-color: transparent !important;\">\n	ก่อตั้งบริษัท โดยกลุ่มบุคคลหลากหลายอาชีพซึ่งมีแนวคิด ในการดำเนินธุรกิจพัฒนาอสังหาริมทรัพย์ด้วยทุนจดทะเบียนเริ่มแรกจำนวน 50 ล้านบาท ประกอบด้วยหุ้นสามัญจำนวน 500,000 หุ้น มูลค่าที่ตราไว้หุ้นละ 100 บาท มีวัตถุประสงค์เพื่อประกอบธุรกิจพัฒนาอสังหาริมทรัพย์ โดยมีสำนักงานตั้งอยู่เลขที่ 667/15 อาคารอรรถบูรณ์ ชั้น 5 ถนนจรัลสนิทวงศ์ แขวงอรุณอมรินทร์ เขตบางกอกน้อย กรุงเทพมหานคร</p>\n', '<p class=\"text_10\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; line-height: 1.7; text-rendering: optimizelegibility; -webkit-tap-highlight-color: transparent !important; font-size: 14px !important; color: rgb(8, 100, 67) !important;\">\n	November</p>\n<p style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(123, 126, 133); -webkit-tap-highlight-color: transparent !important;\">\n	Founded by people from different professions but similar objective to deveop real estate with initial registration capital of 50 million baht comprising of 500,000 shares with the par value of 100 baht. The objective is to operate business in real estate development located at 667/15 Attabon Building 5th Floor, Charansanitwing Road, Arun Amarin, Bangkok Noi, Bangkok.</p>\n', NULL, '2019-11-18 04:50:56'),
(2, 'ปีพุทธศักราช : 2547', 'Year : 2004', NULL, '<p class=\"text_10\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; line-height: 1.7; text-rendering: optimizelegibility; -webkit-tap-highlight-color: transparent !important; font-size: 14px !important; color: rgb(8, 100, 67) !important;\">\n	เดือนมกราคม</p>\n<p style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(123, 126, 133); -webkit-tap-highlight-color: transparent !important;\">\n	เปิดขายอย่างเป็นทางการโครงการ &ldquo;ริชชี่วิลล์ บางบัวทอง&rdquo; เป็นบ้านเดี่ยว 2 ชั้น บนพื้นที่ 13 ไร่ จำนวน 74 ยูนิต ตั้งอยู่ที่อำเภอบางบัวทอง นนทบุรี มูลค่าโครงการประมาณ 160 ล้านบาท</p>\n', '<p class=\"text_10\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; line-height: 1.7; text-rendering: optimizelegibility; -webkit-tap-highlight-color: transparent !important; font-size: 14px !important; color: rgb(8, 100, 67) !important;\">\n	January</p>\n<p style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(123, 126, 133); -webkit-tap-highlight-color: transparent !important;\">\n	Officially opened sale on first project &lsquo;Richyville Bangbuathong&rsquo;, 74 units of a 2-story home on 13 rai located at Bangbuathong Nontaburi value at 160 million baht</p>\n', NULL, '2019-11-18 05:00:21');

-- --------------------------------------------------------

--
-- Table structure for table `profile_info`
--

DROP TABLE IF EXISTS `profile_info`;
CREATE TABLE IF NOT EXISTS `profile_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profile_info`
--

INSERT INTO `profile_info` (`id`, `image`, `title_th`, `title_en`, `title_ch`, `detail_th`, `detail_en`, `detail_ch`, `date`) VALUES
(1, '39863-logo-1-.png', 'ประวัติความเป็นมา', 'Background and Major Developments', NULL, '<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	บริษัท ริชี่ เพลซ 2002 จำกัด (มหาชน) (&ldquo;บริษัท&rdquo;) จดทะเบียนก่อตั้งขึ้นเมื่อวันที่ 18 พฤศจิกายน 2545 ด้วยทุนจดทะเบียนเริ่มแรกจำนวน 50 ล้านบาท โดยกลุ่มบุคคลหลากหลายอาชีพซึ่งมีแนวคิดในการดำเนินธุรกิจพัฒนาอสังหาริมทรัพย์ คล้ายคลึงกัน ภายใต้การบริหารองค์กรโดย ดร.อาภา อรรถบูรณ์วงศ์ เพื่อมุ่งเน้นการพัฒนาโครงการในพื้นที่ที่มีศักยภาพ และเป็นทำเลที่ใกล้แหล่งชุมชน ณ วัน ที่ 29 พฤษภาคม 2558 บริษัทมีทุนจดทะเบียน 985.40 ล้านบาท และมีทุนชำระแล้ว 785.40 ล้านบาท มีสำนักงานตั้งอยู่เลขที่ 667/15 อาคารอรรถบูรณ์ ชั้น 7 ถนนจรัลสนิทวงศ์ แขวงอรุณอมรินทร์ เขตบางกอกน้อย กรุงเทพมหานคร</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	บริษัทเริ่มต้นพัฒนาและเสนอขายโครงการอสังหาริมทรัพย์โครงการแรกเมื่อปี 2547 ภายใต้ชื่อ &ldquo;ริชชี่วิลล์ บางบัวทอง&rdquo; ซึ่งเป็นโครงการบ้านเดี่ยว จำนวน 74 ยูนิต ตั้งอยู่ในอำเภอบางบัวทอง จังหวัดนนทบุรี มูลค่าโครงการประมาณ 160 ล้านบาท หลังจากนั้นได้พัฒนาโครงการอสังหาริมทรัพย์อีกหลายโครงการ ซึ่งตั้งอยู่ในกรุงเทพฯและปริมณฑลทั้งหมด</p>\n', '<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	Richy Place 2002 Public Company Limited was registered on 18 November 2002 with initial registration capital of 50 million baht by group of people from different professions but similar objective to develop real estate under the administration of Dr.Apa Attaboonwong, concentrating on potential locations and close to community.</p>\n<p class=\"font-gray-smoke\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; background-color: rgb(254, 254, 254); -webkit-tap-highlight-color: transparent !important; color: rgb(128, 128, 128) !important;\">\n	The Company has transformed public company in year 2013 with registration capital of 985.40 million baht: 785.40 million baht paid up. On 7 August 2014, the Company registered common stocks with the Stock Exchange of Thailand with 714 registration capital, 714 million baht paid up.</p>\n', NULL, '2019-11-15 12:00:53');

-- --------------------------------------------------------

--
-- Table structure for table `publicize`
--

DROP TABLE IF EXISTS `publicize`;
CREATE TABLE IF NOT EXISTS `publicize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) DEFAULT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `publicize`
--

INSERT INTO `publicize` (`id`, `image`, `title_th`, `title_en`, `title_ch`, `detail_th`, `detail_en`, `detail_ch`, `date`) VALUES
(2, 'cb354-capture.jpg', 'ข้อความเทส', 'Lorem ipsum dolor sit amet,', NULL, '<p>\n	นายพระนาย สุวรรณรัฐ (คนกลาง) ประธานกรรมการ และ ดร.อาภา อรรถบูรณ์วงศ์(คนซ้าย) รองประธานกรรมการและประธานกรรมการบริหาร บริษัท ริชี่ เพลซ 2002 จากัด (มหาชน) หรือ RICHY พร้อมคณะกรรมการ ได้จัดเลี้ยงต้อนรับ นายชัยสิทธิ์ วิริยะเมตตากุล (คนขวา) กรรมการผู้จัดการ และผู้ถือหุ้นใหญ่ ของโรงพยาบาลวิภาวดี และโรงพยาบาลในเครือ ซึ่งรับเป็นที่ปรึกษาคณะกรรมการบริษัทของ RICHY ณ ภัตตาคาร CHEF MAN เมื่อวันที่ 1 เมษายน 2559</p>\n<p>\n	<img alt=\"\" src=\"/richy/uploads/admin/images/pr_20160421181732_T_img2.gif\" style=\"width: 472px; height: 349px;\" /></p>\n', '<p>\n	นายพระนาย สุวรรณรัฐ (คนกลาง) ประธานกรรมการ และ ดร.อาภา อรรถบูรณ์วงศ์(คนซ้าย) รองประธานกรรมการและประธานกรรมการบริหาร บริษัท ริชี่ เพลซ 2002 จากัด (มหาชน) หรือ RICHY พร้อมคณะกรรมการ ได้จัดเลี้ยงต้อนรับ นายชัยสิทธิ์ วิริยะเมตตากุล (คนขวา) กรรมการผู้จัดการ และผู้ถือหุ้นใหญ่ ของโรงพยาบาลวิภาวดี และโรงพยาบาลในเครือ ซึ่งรับเป็นที่ปรึกษาคณะกรรมการบริษัทของ RICHY ณ ภัตตาคาร CHEF MAN เมื่อวันที่ 1 เมษายน 2559</p>\n', NULL, '2019-11-22 08:36:25');

-- --------------------------------------------------------

--
-- Table structure for table `set_announcement`
--

DROP TABLE IF EXISTS `set_announcement`;
CREATE TABLE IF NOT EXISTS `set_announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `set_announcement`
--

INSERT INTO `set_announcement` (`id`, `title_th`, `title_en`, `title_ch`, `file`, `date`) VALUES
(1, 'คำอธิบายและวิเคราะห์ของฝ่ายจัดการ ไตรมาสที่ 3 สิ้นสุดวันที่ 30 ก.ย. 2562 (แก้ไข)', 'Management Discussion and Analysis Quarter 3 Ending 30 Sep 2019 (revise)', NULL, NULL, '2019-11-13');

-- --------------------------------------------------------

--
-- Table structure for table `stock_price`
--

DROP TABLE IF EXISTS `stock_price`;
CREATE TABLE IF NOT EXISTS `stock_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) DEFAULT NULL,
  `detail_th` text,
  `detail_en` text,
  `detail_ch` text,
  `detail2_th` text,
  `detail2_en` text,
  `detail2_ch` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stock_price`
--

INSERT INTO `stock_price` (`id`, `logo`, `detail_th`, `detail_en`, `detail_ch`, `detail2_th`, `detail2_en`, `detail2_ch`, `date`) VALUES
(1, 'cc1a1-logo-1-.png', '<h6 style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; font-size: 1rem; padding: 0px; font-weight: normal; color: rgb(10, 10, 10); text-rendering: optimizelegibility; line-height: 1.4; text-align: center; background-color: rgb(240, 240, 240); -webkit-tap-highlight-color: transparent !important;\">\n	ตลาด :&nbsp;<span class=\"font-mint-green\" style=\"box-sizing: inherit; text-decoration-line: underline; -webkit-tap-highlight-color: transparent !important; color: rgb(29, 159, 104) !important;\">SET</span></h6>\n<h6 style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; font-size: 1rem; padding: 0px; font-weight: normal; color: rgb(10, 10, 10); text-rendering: optimizelegibility; line-height: 1.4; text-align: center; background-color: rgb(240, 240, 240); -webkit-tap-highlight-color: transparent !important;\">\n	กลุ่มอุตสาหกรรม :&nbsp;<span class=\"font-mint-green\" style=\"box-sizing: inherit; text-decoration-line: underline; -webkit-tap-highlight-color: transparent !important; color: rgb(29, 159, 104) !important;\">อสังหาริมทรัพย์และก่อสร้าง</span></h6>\n<h6 style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; font-size: 1rem; padding: 0px; font-weight: normal; color: rgb(10, 10, 10); text-rendering: optimizelegibility; line-height: 1.4; text-align: center; background-color: rgb(240, 240, 240); -webkit-tap-highlight-color: transparent !important;\">\n	หมวดธุรกิจ :&nbsp;<span class=\"font-mint-green\" style=\"box-sizing: inherit; text-decoration-line: underline; -webkit-tap-highlight-color: transparent !important; color: rgb(29, 159, 104) !important;\">พัฒนาอสังหาริมทรัพย์</span></h6>\n', '<h6 style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; font-size: 1rem; padding: 0px; font-weight: normal; color: rgb(10, 10, 10); text-rendering: optimizelegibility; line-height: 1.4; text-align: center; background-color: rgb(240, 240, 240); -webkit-tap-highlight-color: transparent !important;\">\n	ตลาด :&nbsp;<span class=\"font-mint-green\" style=\"box-sizing: inherit; text-decoration-line: underline; -webkit-tap-highlight-color: transparent !important; color: rgb(29, 159, 104) !important;\">SET</span></h6>\n<h6 style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; font-size: 1rem; padding: 0px; font-weight: normal; color: rgb(10, 10, 10); text-rendering: optimizelegibility; line-height: 1.4; text-align: center; background-color: rgb(240, 240, 240); -webkit-tap-highlight-color: transparent !important;\">\n	กลุ่มอุตสาหกรรม :&nbsp;<span class=\"font-mint-green\" style=\"box-sizing: inherit; text-decoration-line: underline; -webkit-tap-highlight-color: transparent !important; color: rgb(29, 159, 104) !important;\">อสังหาริมทรัพย์และก่อสร้าง</span></h6>\n<h6 style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 0px 0px 0.5rem; font-size: 1rem; padding: 0px; font-weight: normal; color: rgb(10, 10, 10); text-rendering: optimizelegibility; line-height: 1.4; text-align: center; background-color: rgb(240, 240, 240); -webkit-tap-highlight-color: transparent !important;\">\n	หมวดธุรกิจ :&nbsp;<span class=\"font-mint-green\" style=\"box-sizing: inherit; text-decoration-line: underline; -webkit-tap-highlight-color: transparent !important; color: rgb(29, 159, 104) !important;\">พัฒนาอสังหาริมทรัพย์</span></h6>\n', NULL, '<h1 class=\"hl-detail-stock font-white\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 27px; margin: 0px; padding: 0px; font-weight: normal; color: rgb(15, 113, 60); text-rendering: optimizelegibility; line-height: 25px; text-align: center; letter-spacing: 0.5px; -webkit-tap-highlight-color: transparent !important;\">\n	<span style=\"box-sizing: inherit; font-weight: 600; line-height: inherit; -webkit-tap-highlight-color: transparent !important;\">ข้อมูลราคาหลักทรัพย์</span></h1>\n<p class=\"sub-detail-stock font-white\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 12px 15px 0px; padding: 0px; font-size: 14px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(85, 85, 85); text-align: center; -webkit-tap-highlight-color: transparent !important;\">\n	ชื่อหุ้น : RICHY</p>\n<p>\n	&nbsp;</p>\n<p class=\"font-white\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin-right: 15px; margin-bottom: 0px; margin-left: 15px; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(85, 85, 85); -webkit-tap-highlight-color: transparent !important; margin-top: 25px !important;\">\n	ข้อมูลที่เปิดเผยในราคาหุ้นที่สำคัญของ บริษัท และปริมาณข้อมูล ซื้อ - ขาย ความเคลื่อนไหวและการเปลี่ยนแปลงของราคาหุ้นราคาล่าสุด รวมถึงผลตอบแทนสินทรัพย์ ผลตอบแทนต่อผู้ถือหุ้น อัตรากำไรสุทธิและกำไรสุทธิของ บริษัท ริชชี่เพลส 2002 จำกัด (มหาชน)</p>\n', '<h1 class=\"hl-detail-stock font-white\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; font-size: 27px; margin: 0px; padding: 0px; font-weight: normal; color: rgb(15, 113, 60); text-rendering: optimizelegibility; line-height: 25px; text-align: center; letter-spacing: 0.5px; -webkit-tap-highlight-color: transparent !important;\">\n	<span style=\"box-sizing: inherit; font-weight: 600; line-height: inherit; -webkit-tap-highlight-color: transparent !important;\">ข้อมูลราคาหลักทรัพย์</span></h1>\n<p class=\"sub-detail-stock font-white\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin: 12px 15px 0px; padding: 0px; font-size: 14px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(85, 85, 85); text-align: center; -webkit-tap-highlight-color: transparent !important;\">\n	ชื่อหุ้น : RICHY</p>\n<p>\n	&nbsp;</p>\n<p class=\"font-white\" style=\"box-sizing: inherit; font-family: Prompt, &quot;Helvetica Neue&quot;, Helvetica, Roboto, Arial, sans-serif; margin-right: 15px; margin-bottom: 0px; margin-left: 15px; padding: 0px; font-size: 16px; line-height: 1.7; text-rendering: optimizelegibility; color: rgb(85, 85, 85); -webkit-tap-highlight-color: transparent !important; margin-top: 25px !important;\">\n	ข้อมูลที่เปิดเผยในราคาหุ้นที่สำคัญของ บริษัท และปริมาณข้อมูล ซื้อ - ขาย ความเคลื่อนไหวและการเปลี่ยนแปลงของราคาหุ้นราคาล่าสุด รวมถึงผลตอบแทนสินทรัพย์ ผลตอบแทนต่อผู้ถือหุ้น อัตรากำไรสุทธิและกำไรสุทธิของ บริษัท ริชชี่เพลส 2002 จำกัด (มหาชน)</p>\n', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

DROP TABLE IF EXISTS `voucher`;
CREATE TABLE IF NOT EXISTS `voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) DEFAULT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`id`, `file`, `title_th`, `title_en`, `title_ch`, `date`) VALUES
(4, '91fa5-20181128140044.pdf', 'รายละเอียดเกี่ยวกับการใช้สิทธิครั้งที่ 5 ของใบสำคัญแสดงสิทธิ (RICHY-W2)', 'รายละเอียดเกี่ยวกับการใช้สิทธิครั้งที่ 5 ของใบสำคัญแสดงสิทธิ (RICHY-W2)', NULL, '2019-11-25 11:28:55'),
(5, NULL, 'แบบฟอร์มแสดงความจำนงการใช้สิทธิที่จะซื้อหุ้นสามัญเพิ่มทุนของบริษัท ริชี่ เพลซ 2002 จำกัด (มหาชน) ครั้งที่ 2 (RICHY-W2)', 'แบบฟอร์มแสดงความจำนงการใช้สิทธิที่จะซื้อหุ้นสามัญเพิ่มทุนของบริษัท ริชี่ เพลซ 2002 จำกัด (มหาชน) ครั้งที่ 2 (RICHY-W2)', NULL, '2019-11-25 11:29:16'),
(6, NULL, 'รายละเอียดเกี่ยวกับการใช้สิทธิครั้งที่ 4 ของใบสำคัญแสดงสิทธิ (RICHY-W2)', 'รายละเอียดเกี่ยวกับการใช้สิทธิครั้งที่ 4 ของใบสำคัญแสดงสิทธิ (RICHY-W2)', NULL, '2019-11-25 11:44:35');

-- --------------------------------------------------------

--
-- Table structure for table `webcasts`
--

DROP TABLE IF EXISTS `webcasts`;
CREATE TABLE IF NOT EXISTS `webcasts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file` varchar(255) DEFAULT NULL,
  `vdo` varchar(255) DEFAULT NULL,
  `title_th` varchar(255) DEFAULT NULL,
  `title_en` varchar(255) DEFAULT NULL,
  `title_ch` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `webcasts`
--

INSERT INTO `webcasts` (`id`, `file`, `vdo`, `title_th`, `title_en`, `title_ch`, `date`) VALUES
(1, '9e6c2-20181128140044.pdf', NULL, 'บริษัทจดทะเบียนพบผู้ลงทุน สำหรับผลประกอบการไตรมาส 3/2561', 'Opportunity Day Quarter 3/2018', NULL, '2019-11-20 07:35:49'),
(2, NULL, '1165f-movie.mp4', 'Lorem ipsum dolor sit amet,', 'Lorem ipsum dolor sit amet,', NULL, '2019-11-20 08:22:59');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
