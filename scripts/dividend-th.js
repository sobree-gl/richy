(function () {
	var files = ["https://code.highcharts.com/stock/highstock.js", "https://code.highcharts.com/highcharts-more.js", "https://code.highcharts.com/highcharts-3d.js", "https://code.highcharts.com/modules/data.js", "https://code.highcharts.com/modules/funnel.js", "https://code.highcharts.com/modules/annotations.js", "https://code.highcharts.com/modules/solid-gauge.js"],
		loaded = 0;
	if (typeof window["HighchartsEditor"] === "undefined") {
		window.HighchartsEditor = {
			ondone: [cl],
			hasWrapped: false,
			hasLoaded: false
		};
		include(files[0]);
	} else {
		if (window.HighchartsEditor.hasLoaded) {
			cl();
		} else {
			window.HighchartsEditor.ondone.push(cl);
		}
	}

	function isScriptAlreadyIncluded(src) {
		var scripts = document.getElementsByTagName("script");
		for (var i = 0; i < scripts.length; i++) {
			if (scripts[i].hasAttribute("src")) {
				if ((scripts[i].getAttribute("src") || "").indexOf(src) >= 0 || (scripts[i].getAttribute("src") === "http://code.highcharts.com/highcharts.js" && src === "https://code.highcharts.com/stock/highstock.js")) {
					return true;
				}
			}
		}
		return false;
	}

	function check() {
		if (loaded === files.length) {
			for (var i = 0; i < window.HighchartsEditor.ondone.length; i++) {
				try {
					window.HighchartsEditor.ondone[i]();
				} catch (e) {
					console.error(e);
				}
			}
			window.HighchartsEditor.hasLoaded = true;
		}
	}

	function include(script) {
		function next() {
			++loaded;
			if (loaded < files.length) {
				include(files[loaded]);
			}
			check();
		}
		if (isScriptAlreadyIncluded(script)) {
			return next();
		}
		var sc = document.createElement("script");
		sc.src = script;
		sc.type = "text/javascript";
		sc.onload = function () {
			next();
		};
		document.head.appendChild(sc);
	}

	function each(a, fn) {
		if (typeof a.forEach !== "undefined") {
			a.forEach(fn);
		} else {
			for (var i = 0; i < a.length; i++) {
				if (fn) {
					fn(a[i]);
				}
			}
		}
	}
	var inc = {},
		incl = [];
	each(document.querySelectorAll("script"), function (t) {
		inc[t.src.substr(0, t.src.indexOf("?"))] = 1;
	});

	function cl() {
		if (typeof window["Highcharts"] !== "undefined") {
			var options = {
				"title": {
					"text": "นโยบายจ่ายปันผล"
				},
				"subtitle": {
					"text": ""
				},
				"exporting": {},
				"series": [{
					"name": "จำนวนเงินปันผลต่อหุ้น",
					"turboThreshold": 0
				}],
				"xAxis": [{
					"type": "category",
					"uniqueNames": false,
					"title": {},
					"labels": {
						"format": "{value}"
					}
				}],
				"data": {
					"csv": "\"Category\";\"จำนวนเงินปันผลต่อหุ้น\"\n2561;0.0087\n2560;0.0063\n2559;0.055\n2558;0.04",
					"googleSpreadsheetKey": false,
					"googleSpreadsheetWorksheet": false
				},
				"chart": {
					"type": "column",
					"inverted": false
				},
				"yAxis": [{
					"allowDecimals": false,
					"title": {
						"text": "บาท/หุ้น",
						"style": "{ \"color\": \"#666666\" }"
					},
					"labels": {},
					"type": "logarithmic"
				}],
				"tooltip": {},
				"plotOptions": {
					"series": {
						"animation": false
					}
				},
				"legend": {},
				"colors": ["#1d9f68", "#00663b", "#9d7f49", "#303438", "#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1"],
				"credits": {
					"enabled": false
				}
			};
			/*
			// Sample of extending options:
			Highcharts.merge(true, options, {
			    chart: {
			        backgroundColor: "#bada55"
			    },
			    plotOptions: {
			        series: {
			            cursor: "pointer",
			            events: {
			                click: function(event) {
			                    alert(this.name + " clicked\n" +
			                          "Alt: " + event.altKey + "\n" +
			                          "Control: " + event.ctrlKey + "\n" +
			                          "Shift: " + event.shiftKey + "\n");
			                }
			            }
			        }
			    }
			});
			*/
			new Highcharts.Chart("highcharts-18e59108-26a7-408a-81fc-cb993af29af1", options);
		}
	}
})();
