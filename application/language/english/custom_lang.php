<?php

$lang['home'] = "IR Home";
$lang['IRMenu'] = "IR Menu";
$lang['webcasts'] = "Webcasts & Presentation";
$lang['IRCalendar'] = "IR Calendar";
$lang['NewsActivities'] = "News & Activities";
$lang['Presentation'] = "Presentation";
$lang['Newsclipping'] = "News clipping";
$lang['NewsUpdate'] = "News Update";
$lang['SETAnnoucement'] = "SET Annoucement";
$lang['StockInformation'] = "Stock Information";
$lang['StockQuote'] = "Stock Quote";
$lang['IRContact'] = "IR Contact";
$lang['CorporateInfo'] = "Corporate Info";
$lang['ChairmanStatement'] = "Chairman's Statement";
$lang['History'] = "History";
$lang['VisionMission'] = "Vision & Mission";
$lang['BoardofDirector'] = "Board of Director";
$lang['OrganizationChart'] = "Organization Chart";
$lang['CorporateGovernance'] = "Corporate Governance";
$lang['ShareholderInfo'] = "Shareholder Info";
$lang['MeetingofShareholder'] = "Meeting of Shareholder";
$lang['MajorShareholder'] = "Major Shareholder";
$lang['Profile'] = "Profile";
$lang['PublicRelations'] = "Public Relations";
$lang['DividendPolicy'] = "Dividend Policy";



