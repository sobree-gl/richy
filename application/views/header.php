<div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99"
        style="transform: translate3d(100%, 0px, 0px);">
        <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div>
</div>

<div id="loadpage">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>

<div data-sticky-container="" class="sticky-container" style="height: 81.375px;">
    <header class="igray sticky is-anchored is-at-top" data-sticky="" data-margin-top="0" data-sticky-on="small"
        data-resize="734c9g-sticky" data-mutate="734c9g-sticky" data-e="kw1f4i-e" data-events="resize"
        style="max-width: 1903px; margin-top: 0px; bottom: auto; top: 0px;">
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell small-3 medium-2"> <a href="https://www.richy.co.th/th/home_page" class="i-logo">
                        <img src="<?=base_url();?>images/logo.svg"></a> </div>

                <!-- ................................................ Menu top ........................................................... -->
                <div class="cell small-3 medium-8">
                    <div class="top-web"></div>

                    <ul class="vertical medium-horizontal menu th dropdown"
                        data-responsive-menu="drilldown medium-dropdown" role="menubar" data-e="rhd1vq-e"
                        data-mutate="rmyu9n-responsive-menu">


                        <li class="" role="menuitem"> <a href="https://www.richy.co.th/th/home_page">หน้าหลัก</a>
                            <span></span> </li>
                        <li class="is-dropdown-submenu-parent opens-right" role="menuitem" aria-haspopup="true"
                            aria-label="โครงการริชี่"> <a
                                href="https://www.richy.co.th/th/richy_project">โครงการริชี่</a> <span></span>
                            <ul class="vertical menu submenu is-dropdown-submenu first-sub" data-submenu="" role="menu">
                                <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"> <a
                                        href="https://www.richy.co.th/th/richy_project">โครงการริชี่ทั้งหมด</a>
                                </li>
                                <li role="menuitem"
                                    class="is-dropdown-submenu-parent is-submenu-item is-dropdown-submenu-item opens-right"
                                    aria-haspopup="true" aria-label=" คอนโด"> <a
                                        href="https://www.richy.co.th/th/richy_project/condo"> <i
                                            class="icon-i-condo"></i>คอนโด</a>
                                    <ul class="vertical menu submenu is-dropdown-submenu" data-submenu="" role="menu">
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/condo/เดอะริช+พระราม+9+-+ศรีนครินทร์">เดอะริช
                                                พระราม 9 - ศรีนครินทร์</a></li>
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/condo/เดอะริช+เพลินจิต+-+นานา">เดอะริช
                                                เพลินจิต - นานา</a></li>
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/condo/เดอะริช+สาทร+-+ตากสิน">เดอะริช
                                                สาทร - ตากสิน</a></li>
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/condo/The+8+Collection+วิสุทธิกษัตริย์">The
                                                8 Collection วิสุทธิกษัตริย์</a></li>
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/condo/ริชพาร์ค+เทอมินอล+หลักสี่">ริชพาร์ค
                                                เทอมินอล หลักสี่</a></li>
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/condo/ริชพาร์ค+ทริปเปิ้ลสเตชั่น+ศรีนครินทร์++">ริชพาร์ค
                                                ทริปเปิ้ลสเตชั่น ศรีนครินทร์ </a></li>
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/condo/ริชพาร์ค+เตาปูน+อินเตอร์เชนจ์">ริชพาร์ค
                                                เตาปูน อินเตอร์เชนจ์</a></li>
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/condo/ริชพาร์ค+เจ้าพระยา+เชิงสะพานพระนั่งเกล้า">ริชพาร์ค
                                                เจ้าพระยา เชิงสะพานพระนั่งเกล้า</a></li>
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/condo/ริชพาร์ค+บางซ่อน+สเตชั่น">ริชพาร์ค
                                                บางซ่อน สเตชั่น</a></li>
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/condo/เลอริช+พระราม+3">เลอริช
                                                พระราม 3</a></li>
                                    </ul>
                                </li>
                                <li role="menuitem"
                                    class="is-dropdown-submenu-parent is-submenu-item is-dropdown-submenu-item opens-right"
                                    aria-haspopup="true" aria-label=" บ้าน"><a
                                        href="https://www.richy.co.th/th/richy_project/home"> <i
                                            class="icon-i-house"></i>บ้าน</a>
                                    <ul class="vertical menu submenu is-dropdown-submenu" data-submenu="" role="menu">
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/home/เดอะริช+บิซโฮม+สุขุมวิท+105">เดอะริช
                                                บิซโฮม สุขุมวิท 105</a></li>
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/home/+เดอะริชวิลล์+ราชพฤกษ์+-+รัตนาธิเบศร์">
                                                เดอะริชวิลล์ ราชพฤกษ์ - รัตนาธิเบศร์</a></li>
                                    </ul>
                                </li>
                                <li role="menuitem"
                                    class="is-dropdown-submenu-parent is-submenu-item is-dropdown-submenu-item opens-right"
                                    aria-haspopup="true" aria-label="
        โครงการอื่น ๆ"><a href="https://www.richy.co.th/th/richy_project/other_project"> <i
                                            class="icon-i-other"></i>โครงการอื่น ๆ</a>
                                    <ul class="vertical menu submenu is-dropdown-submenu" data-submenu="" role="menu">
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/other/เดอะริช+อเวนิว+ดำรงรักษ์">เดอะริช
                                                อเวนิว ดำรงรักษ์</a></li>
                                        <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                                href="https://www.richy.co.th/th/project/other/อาคารอรรถบูรณ์">อาคารอรรถบูรณ์</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="" role="menuitem"> <a href="https://www.richy.co.th/th/promotion">โปรโมชั่น</a>
                            <span></span> </li>
                        <li class="act-i" role="menuitem"> <a href="<?=site_url('investor');?>">นักลงทุนสัมพันธ์</a> <span></span>
                        </li>
                        <li class="is-dropdown-submenu-parent opens-right" role="menuitem" aria-haspopup="true"
                            aria-label="ข้อมูลองค์กร "> <a href="https://www.richy.co.th/th/corporate">
                                ข้อมูลองค์กร</a> <span></span>
                            <ul class="vertical menu submenu is-dropdown-submenu first-sub" data-submenu="" role="menu">
                                <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                        href="https://www.richy.co.th/th/corporate/about_us">เกี่ยวกับเรา</a></li>
                                <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                        href="https://www.richy.co.th/th/corporate/career">ร่วมงานกับเรา</a></li>
                                <li role="menuitem" class="is-submenu-item is-dropdown-submenu-item"><a
                                        href="https://www.richy.co.th/th/corporate/broker">ตัวแทนจำหน่าย</a></li>
                            </ul>
                        </li>
                        <li class="" role="menuitem"> <a href="https://www.richy.co.th/th/contact">ติดต่อเรา</a>
                            <span></span> </li>
                    </ul>
                </div>
                <!-- ............................................... End Menu top ........................................................ -->


                <div class="cell small-6 medium-2">
                    <div class="top-web"></div>
                    <!-- ................................................ language ........................................................... -->
                    <div class="group-t">
                        <div class="group-lang">
                            <!--<a href="organization.htm" class="active">TH</a> | <a href="organization_en.htm">EN</a> | <a href="#0">CH</a> </div>-->

                            <a class="<?php if($this->lang->lang()=='th'){echo 'active';} ?>" href="<?=site_url().$this->lang->switch_uri('th')?>">TH</a> | 
                            <a class="<?php if($this->lang->lang()=='en'){echo 'active';} ?>" href="<?=site_url().$this->lang->switch_uri('en')?>">EN</a> | 
                            <a class="" href="javascript:void(0);">CH</a>

                        </div>
                        <a href="https://www.facebook.com/rp.co.th" data-tooltip="" class="bottom facebook-top has-tip"
                            tabindex="4" title="" target="_blank" aria-describedby="fl3gxd-tooltip"
                            data-yeti-box="fl3gxd-tooltip" data-toggle="fl3gxd-tooltip" data-resize="fl3gxd-tooltip"
                            data-e="mi6ocb-e" data-events="resize"><i class="fab fa-facebook"></i></a>
                        <div class="b-search" data-open="search1" aria-controls="search1" aria-haspopup="true"
                            tabindex="0"><i class="fas fa-search"></i></div>
                        <div class="reveal" id="search1" data-reveal="" data-animation-in="slide-in-down"
                            data-animation-out="slide-out-up" role="dialog" aria-hidden="false" data-yeti-box="search1"
                            data-resize="search1" data-e="rvdbqx-e" tabindex="-1">
                            <div class="search-property" style="margin-top:50px;">
                                <form id="searchMain" action="https://www.richy.co.th/th/search/web" method="get">
                                    <div class="input-group">
                                        <input class="input-group-field" type="text" name="q"
                                            placeholder="ค้นหาโครงการ">
                                        <div class="input-group-button">
                                            <input type="hidden" name="hl" value="th">
                                            <input type="submit" class="button" value="search">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <button class="close-button" data-close="" aria-label="Close modal" type="button"> <span
                                    aria-hidden="true">×</span> </button>
                        </div>
                        <div class="ham">
                            <div class="bar1"></div>
                            <div class="bar2"></div>
                            <div class="bar3"></div>
                        </div>
                    </div>

                    <!-- ............................................... End language top ........................................................ -->

                </div>
            </div>
    </header>
</div>