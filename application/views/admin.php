<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ADMIN</title>
	<?php foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
	<?php endforeach; ?>
</head>

<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="<?php echo site_url('admin/customers')?>">Navbar</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="<?php echo site_url('admin/customers')?>">หน้าหลักนักลงทุนสัมพันธ์</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo site_url('admin/voucher')?>">การจัดการใบสำคัญ</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						ข้อมูลบริษัท
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo site_url('admin/chairman')?>">สารจากประธานกรรมการ</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/profile_info')?>">ประวัติความเป็นมา Info</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/profile')?>">ประวัติความเป็นมา</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/passion')?>">วิสัยทัศน์และพันธกิจ</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/board')?>">คณะกรรมการบริษัทและผู้บริหาร</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/organization')?>">โครงสร้างองค์กร</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo site_url('admin/governance')?>">การกำกับดูแลกิจการ</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						ข้อมูลสำหรับผู้ถือหุ้น
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo site_url('admin/corporate')?>">ข้อมูลทั่วไปของบริษัท</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/major')?>">โครงสร้างผู้ถือหุ้น</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/major_info')?>">โครงสร้างผู้ถือหุ้น info</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/meeting_category')?>">การประชุมผู้ถือหุ้น</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/dividend_info')?>">นโยบายและการจ่ายเงินปันผล info</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/dividend')?>">นโยบายและการจ่ายเงินปันผล</a>
						<a class="dropdown-item disabled" href="javascript:;">สรุปข้อสนเทศ</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo site_url('admin/webcasts')?>">ข้อมูลนำเสนอแบบมัลติมีเดีย</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="javascript:;">บทวิเคราะห์</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						ข่าวสารและกิจกรรม
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo site_url('admin/calendar')?>">ปฏิทินกิจกรรม</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/news_update')?>">ข่าวสารล่าสุด</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/news_clipping')?>">ข่าวจากหนังสือพิมพ์</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/set_announcement')?>">ข่าวประกาศจากตลาดหลักทรัพย์</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/publicize')?>">ข่าวประชาสัมพันธ์</a>
						<a class="dropdown-item disabled" href="javascript:;">ข่าวเผยแพร่</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						ข้อมูลราคาหลักทรัพย์
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo site_url('admin/stock_price')?>">ราคาหลักทรัพย์ล่าสุด</a>
						<a class="dropdown-item disabled" href="javascript:;">ราคาหลักทรัพย์ย้อนหลัง</a>
						<a class="dropdown-item disabled" href="javascript:;">เครื่องคำนวณการลงทุน</a>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo site_url('admin/ir_contact')?>">ติดต่อนักลงทุนสัมพันธ์</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="javascript:;" id="navbarDropdown" role="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						สอบถามข้อมูล
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="<?php echo site_url('admin/complaints_category')?>">หมวดหมู่ช่องทางรับเรื่องร้องเรียน</a>
						<a class="dropdown-item" href="<?php echo site_url('admin/complaints_save')?>">ช่องทางรับเรื่องร้องเรียน</a>
					</div>
				</li>
			</ul>
		</div>
	</nav>
	<hr>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<?php echo $output; ?>
			</div>
		</div>
	</div>
	<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
	<?php endforeach; ?>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
		integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
	</script>
</body>

</html>