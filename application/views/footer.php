<!-- ................................................ CRM ........................................................... -->
<section class="igray">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12">
                <!-- <div class="hr-i"></div> -->
            </div>
            <div class="cell small-4 medium-2 medium-offset-2">
                <!--<div class="group-crm"> <a data-open="crm" aria-controls="crm" aria-haspopup="true" tabindex="0"> <img src="<?=base_url();?>images/i-callcenter.svg" width="70"> CRM </a>-->
                <div class="group-crm" style="color: #0D3D21;"><img src="<?=base_url();?>images/i-callcenter.svg"
                        width="70"> CRM
                    <div>
                        <div>
                            <div>
                                <div class="reveal" id="crm" data-reveal="" role="dialog" aria-hidden="false"
                                    data-yeti-box="crm" data-resize="crm" data-e="cuw2ik-e" tabindex="-1"
                                    style="display: block; top: 96px;">
                                    <div class="grid-container">
                                        <div class="grid-x">
                                            <div class="cell small-12">
                                                <h1 style="font-size:24px;margin:0;font-weight:bold">CRM - Customer
                                                    Relationship Management</h1>
                                                <hr>
                                                <!-- <form action="http://www.rp.co.th/rich_in_living/en/crm/sendmail" name="form_contact" id="form_contact" class="form_contact" method="post" accept-charset="utf-8">
		 -->

                                                <form action="https://www.richy.co.th/th/crm/sendmail" method="post"
                                                    accept-charset="utf-8" id="formCRM" class="form_contact"
                                                    data-abide="" novalidate="" data-e="i613zg-e">
                                                    <select name="subject" required=""
                                                        style="border:1px solid #ccc;width:100%;">
                                                        <option value="">เลือกประเภทติดต่อ</option>
                                                        <option value="เพื่อนแนะนำเพื่อน">เพื่อนแนะนำเพื่อน</option>
                                                        <option value="แจ้งปัญหาข้อมูล">แจ้งปัญหาข้อมูล</option>
                                                        <option value="ติดต่อเข้าเยี่ยมชมโครงการ">นัดเยี่ยมชมโครงการ
                                                        </option>
                                                        <option value="สอบถามรายละเอียดโครงการ">สอบถามรายละเอียดโครงการ
                                                        </option>
                                                        <option value="ซื้อเพื่อลงทุน">ซื้อเพื่อลงทุน</option>
                                                        <option value="สอบถามโปรโมชั่น">สอบถามโปรโมช่ัน</option>
                                                        <option value="อื่นๆ">อื่นๆ</option>
                                                    </select>
                                                    <div class="grid-x grid-margin-x">
                                                        <div class="cell small-12 medium-6">
                                                            <input type="text" name="name" value="" placeholder=" Name"
                                                                class="xxwide text input" required="required">
                                                        </div>
                                                        <div class="cell small-12 medium-6">
                                                            <input type="text" name="surname" value=""
                                                                placeholder=" Surname" class=" xxwide text  input"
                                                                required="required">
                                                        </div>
                                                    </div>
                                                    <div class="grid-x grid-margin-x">
                                                        <div class="cell small-12 medium-6">
                                                            <div id="contact_email">
                                                                <input type="text" name="email" value=""
                                                                    placeholder=" Email" class="xxwide text  input"
                                                                    required="required">
                                                            </div>
                                                        </div>
                                                        <div class="cell small-12 medium-6">
                                                            <div id="contact_phone">
                                                                <input type="text" name="phone" value=""
                                                                    placeholder=" Phone " class="xxwide text  input"
                                                                    required="required">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="grid-x grid-margin-x">
                                                        <div class="cell small-12 ">
                                                            <div id="contact_content" class="field">
                                                                <textarea name="detail" placeholder=" Message"
                                                                    class="input textarea"
                                                                    style="width:100%;min-height:120px;margin-top:10px;"
                                                                    required="required"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div style="float:right;margin-top:20px;" id="button_submit">
                                                        <div class="medium info btn">
                                                            <button class="button">Submit</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="grid-x grid-margin-x">
                                                <div class="cell small-12 ">
                                                    <h2 style="font-size:1.2rem;font-weight:bold;">CRM /
                                                        ติดต่อลูกค้าสัมพันธ์</h2>
                                                    <span>02 886 1817 ต่อ(ext) 111 </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="close-button" data-close="" aria-label="Close modal"
                                        type="button"><span aria-hidden="true">×</span></button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                </div>
            </div>
            <div class="cell small-8 medium-6">
                <div class="box-newsletter">
                    <form id="subscribe" data-abide="" novalidate="" data-e="8x848e-e">
                        <div class="input-group">
                            <input class="input-group-field" name="subscribe" type="email"
                                placeholder="กรอกอีเมล รับข่าวสาร" required="">
                            <div class="input-group-button">
                                <input type="button" class="button" value="ส่งข้อมูล">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="loadsend">
    <div class="cen-box"> <i class="far fa-paper-plane" id="i1"></i> <i class="far fa-check-circle" id="i2"
            style="display:none;"></i>
        <div class="textcen">กำลังส่งข้อมูล ...</div>
    </div>
</div>


<script>
    Foundation.Abide.defaults.validators.requiredCaptcha = function ($el) {
        if ($('#recapVal').val() != '') {
            return true;
        }
        return false;
    };

    $(document).foundation();

    function subscribe(frm) {
        $('#i1').show();
        $('#i2').hide();
        $('.textcen').text('กำลังส่งข้อมูล ...');
        $('.loadsend').fadeIn();
        $('body').addClass('noscroll');

        var values = $('#' + frm).serialize();
        $.ajax({
            url: "https://www.richy.co.th/th/mailer/subscribe",
            type: "post",
            data: values,
            success: function (res) {
                // you will get response from your php page (what you echo or print)
                console.log(res);

                setTimeout(function () {
                    $('#i1').hide();
                    $('#i2').fadeIn();
                    $('.textcen').delay(2100).text('ส่งเรียบร้อย');
                    $('.loadsend').delay(2100).fadeOut();

                    $('body').removeClass('noscroll');
                    $('#' + frm)[0].reset();
                }, 3000);


            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('.loadsend').fadeOut();
                setTimeout(function () {
                    $('body').removeClass('noscroll');
                    swal({
                        type: 'warning',
                        text: 'Email นี้ใช้งานแล้ว',
                        confirmButtonColor: '#00804A',
                    })
                }, 1000);
            }

        });
    }

    function contactform(frm) {
        $('#i1').show();
        $('#i2').hide();
        $('.textcen').text('กำลังส่งข้อมูล ...');
        $('.loadsend').fadeIn();
        $('body').addClass('noscroll');
        var values = $('#' + frm).serialize();

        $.ajax({
            url: "https://www.richy.co.th/th/mailer/contact",
            type: "post",
            data: values,
            success: function (res) {
                setTimeout(function () {
                    $('#i1').hide();
                    $('#i2').fadeIn();
                    $('.textcen').delay(2100).text('ส่งเรียบร้อย');
                    $('.loadsend').delay(2100).fadeOut();
                    $('body').removeClass('noscroll');
                    $('#' + frm)[0].reset();
                    grecaptcha.reset();
                }, 3000);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }

        });
    }

    function crmform(frm) {
        $('#i1').show();
        $('#i2').hide();
        $('.textcen').text('กำลังส่งข้อมูล ...');
        $('.loadsend').fadeIn();
        $('body').addClass('noscroll');
        var values = $('#' + frm).serialize();
        $.ajax({
            url: "https://www.richy.co.th/th/crm/sendmail",
            type: "post",
            data: values,
            success: function (res) {
                setTimeout(function () {

                    $('#i1').hide();
                    $('#i2').fadeIn();
                    $('.textcen').delay(2100).text('ส่งเรียบร้อย');
                    $('.loadsend').delay(2100).fadeOut();
                    $('body').removeClass('noscroll');
                    $('#' + frm)[0].reset();
                }, 3000);

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }

        });
    }
    /*
    $(document)
      .on("invalid.zf.abide", function(ev,elem) {
        console.log("Field id "+ev.target.id+" is invalid");
      })
      .on("valid.zf.abide", function(ev,elem) {
        console.log("Field name "+elem.attr('name')+" is valid");
      })
      // form validation failed
      .on("forminvalid.zf.abide", function(ev,frm) {
        console.log("Form id "+ev.target.id+" is invalid");
      })
      // form validation passed, form will submit if submit event not returned false
      .on("formvalid.zf.abide", function(ev,frm) {
        console.log("Form id "+frm.attr('id')+" is valid");
        // ajax post form
         var idF = frm.attr('id');
        if(idF === "contactForm"){
          contactform(idF);
          console.log(idF)
          }

          if(idF === "subscribe"){
            subscribe(idF);
            console.log(idF)
          }

          if(idF === "formCRM"){
            crmform(idF);
            console.log(idF)
          }

      }).on("submit", function(ev) {
        if(ev.target.id === "locate" || ev.target.id === "formBroker" || ev.target.id === "searchMain" || ev.target.id === "searchC" || ev.target.id === "price" || ev.target.id === "route" ){

         }else{
          ev.preventDefault();
         }

        console.log("Submit for form id "+ev.target.id+" intercepted");
      });

    $("form#locate").submit(function (ev) {
      //ev.preventDefault();
      var idm = $.trim($('.sr0').val());
     if (idm  === '') {
      swal({
        type: 'error',
        text: 'กรุณาเลือกโครงการ!',
        confirmButtonColor: '#00804A',
        });
        return false;
     }
    });

    $("form#price").submit(function (ev) {
      //ev.preventDefault();
      var idm = $.trim($('.sr1').val());
    if(idm  === '') {
      swal({
        type: 'error',
        text: 'กรุณาเลือกราคา!',
        confirmButtonColor: '#00804A',
       });
        return false;
     }
    });

    $("form#route").submit(function (ev) {
      var idm = $.trim($('.sr2').val());
      if (idm  === '') {
        swal({type: 'error',text: 'กรุณาเลือกสถานี!',confirmButtonColor: '#00804A'});
        return false;
      }
    });*/
</script>


<div class="reveal-overlay">
    <div class="reveal" id="crm" data-reveal="" role="dialog" aria-hidden="true" data-yeti-box="crm" data-resize="crm"
        data-e="lpj7eh-e" data-events="resize">
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell small-12">
                    <h1 style="font-size:24px;margin:0;font-weight:bold">CRM - Customer Relationship Management</h1>
                    <hr>
                    <form action="https://www.richy.co.th/th/crm/sendmail" method="post" accept-charset="utf-8"
                        id="formCRM" class="form_contact" data-abide="" novalidate="" data-e="hombcj-e">
                        <select name="subject" required="" style="border:1px solid #ccc;">
                            <option value="">เลือกประเภทติดต่อ</option>
                            <option value="เพื่อนแนะนำเพื่อน">เพื่อนแนะนำเพื่อน</option>
                            <option value="แจ้งปัญหาข้อมูล">แจ้งปัญหาข้อมูล</option>
                            <option value="ติดต่อเข้าเยี่ยมชมโครงการ">นัดเยี่ยมชมโครงการ</option>
                            <option value="สอบถามรายละเอียดโครงการ">สอบถามรายละเอียดโครงการ</option>
                            <option value="ซื้อเพื่อลงทุน">ซื้อเพื่อลงทุน</option>
                            <option value="สอบถามโปรโมชั่น">สอบถามโปรโมช่ัน</option>
                            <option value="อื่นๆ">อื่นๆ</option>
                        </select>
                        <div class="grid-x grid-margin-x">
                            <div class="cell small-12 medium-6">
                                <input type="text" name="name" value="" placeholder=" Name" class="xxwide text input"
                                    required="required">
                            </div>
                            <div class="cell small-12 medium-6">
                                <input type="text" name="surname" value="" placeholder=" Surname"
                                    class=" xxwide text  input" required="required">
                            </div>
                        </div>
                        <div class="grid-x grid-margin-x">
                            <div class="cell small-12 medium-6">
                                <div id="contact_email">
                                    <input type="text" name="email" value="" placeholder=" Email"
                                        class="xxwide text  input" required="required">
                                </div>
                            </div>
                            <div class="cell small-12 medium-6">
                                <div id="contact_phone">
                                    <input type="text" name="phone" value="" placeholder=" Phone "
                                        class="xxwide text  input" required="required">
                                </div>
                            </div>
                        </div>
                        <div class="grid-x grid-margin-x">
                            <div class="cell small-12 ">
                                <div id="contact_content" class="field">
                                    <textarea name="detail" placeholder=" Message" class="input textarea"
                                        style="width:100%;min-height:120px;margin-top:10px;"
                                        required="required"></textarea>
                                </div>
                            </div>
                        </div>
                        <div style="float:right;margin-top:20px;" id="button_submit">
                            <div class="medium info btn">
                                <button class="button">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="grid-x grid-margin-x">
                    <div class="cell small-12 ">
                        <h2 style="font-size:1.2rem;font-weight:bold;">CRM / ติดต่อลูกค้าสัมพันธ์</h2>
                        <span>02 886 1817 ต่อ(ext) 111 </span>
                    </div>
                </div>
            </div>
        </div>
        <button class="close-button" data-close="" aria-label="Close modal" type="button"><span
                aria-hidden="true">×</span></button>
    </div>
</div>

<!-- ............................................... End CRM ........................................................ -->


<!-- ................................................ footer ........................................................... -->
<footer>
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12 medium-4 text-center">
                <div class="b-f">
                    <div class="text-callcenter text-right">ติดต่อเจ้าหน้าที่</div>
                    <div class="tel-contact"> <span> <img class="wow tada" data-wow-iteration="10"
                                src="<?=base_url();?>images/i-tel.svg" alt="" width="40"
                                style="visibility: visible; animation-iteration-count: 10; animation-name: tada;"></span>
                        <a href="tel:+6628861817">1343</a> </div>
                    <ul class="menu-foot">
                        <li> <a href="https://www.richy.co.th/th/contact">ติดต่อริชี่</a> </li>
                        <li> <a href="https://www.richy.co.th/th/corporate/career">สมัครงาน</a> </li>
                        <li> <a href="https://www.richy.co.th/th/corporate/organization_structure">SITE MAP</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="cell small-12 medium-8 text-center">
                <ul class="social-foot">
                    <li> <a target="_blank" href="https://www.facebook.com/richyplace2002/">
                            <div class="i-c">
                                <!-- <span class="icon-i-facebook"></span> -->
                                <i class="fab fa-facebook"></i> </div>
                            <!-- <span>RICHY PLACE</span> -->
                        </a> </li>
                    <li> <a target="_blank" href="https://line.me/ti/p/%40gjf0556p">
                            <div class="i-c">
                                <!-- <span class="icon-i-line"></span> -->
                                <i class="fab fa-line"></i> </div>
                            <!-- <span>@RICHY PLACE</span> -->
                        </a> </li>
                    <li> <a target="_blank"
                            href="https://www.youtube.com/channel/UCUh7x2RR6KTTnSepmxV_olA?feature=watch">
                            <div class="i-c">
                                <!-- <span class="icon-i-youtube"></span> -->
                                <i class="fab fa-youtube-square"></i> </div>
                            <!-- <span>RICHY CHANNEL</span> -->
                        </a> </li>
                    <li> <a target="_blank" href="https://www.instagram.com/Richyplace/">
                            <div class="i-c">
                                <!-- <span class="icon-i-insta"></span> -->
                                <i class="fab fa-instagram"></i> </div>
                            <!-- <span>RICHY CHANNEL</span> -->
                        </a> </li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<div class="copyright">
    <p> Copyright © 2013, Richy Place 2002 PCL. All right reserved.</p>
</div>

<a href="#" id="back-to-top" class="back-to-top" title="Back to top"></a>
<script src="<?=base_url();?>scripts/wow.js"></script>
<script>
    $('.ham').click(function () {
        $(this).toggleClass('change');
        $('.menu').toggle();
        $('body').toggleClass('noscroll');
    });
    $('.chat .right-arrow').on('click', function () {
        $('.in-chat').slideToggle();
        $(this).toggleClass('open');
    });
    if ($('.back-to-top').length) {
        var scrollTrigger = 500, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('.back-to-top').addClass('ishow');
                } else {
                    $('.back-to-top').removeClass('ishow');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('.back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
    }
</script>
<!-- WhatsHelp.io widget -->
<script type="text/javascript" async="">
    (function () {
        var options = {
            call: "+6628861817",
            facebook: "327568079082", // Facebook page ID
            line: "//line.me/ti/p/%40gjf0556p",
            email: "contact@rp.co.th", // Line QR code URL
            company_logo_url: "https://www.richy.co.th/img/logo-80.jpg",
            greeting_message: "สวัสดีค่ะ สอบถามรายละเอียดสินค้าและบริการได้ที่นี่ค่ะ.",
            call_to_action: "ติดต่อสอบถามเพิ่มเติม", // Call to action
            button_color: "#129BF4", // Color of button
            position: "right", // Position may be 'right' or 'left'
            order: "facebook,line,call,email", // Order of buttons

        };
        var proto = document.location.protocol,
            host = "whatshelp.io",
            url = proto + "//static." + host;
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () {
            WhWidgetSendButton.init(host, proto, options);

        };
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->

<!-- ............................................... footer ........................................................ -->