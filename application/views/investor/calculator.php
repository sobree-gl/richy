<!---------------------------------- Content ---------------------------------------->
<section>
    <style>
        input[type="text"] {
            text-align: right;
        }

        table tbody tr:nth-child(even) {
            border-bottom: 0;
            background-color: #fff;
        }

        table {
            border-collapse: inherit;
        }
    </style>
    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong>Investment Calculator</strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('StockInformation')?></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green">Investment Calculator</span></p>
        </div>
        <form name="calculator" METHOD="POST" ACTION="https://www.irplus.in.th/Listed/RICHY/calculator.asp"
            class="bgcolor_body_detail">

            <div class="webcasts">
                <table width="100%" cellspacing="1" cellpadding="3" border="0" class="webcasts" align="center">
                    <tr align="left">
                        <td colspan="3">
                            <font class="font_calculator">[ Instructions: ]</font><br><br>
                            To estimate your profit and loss, please fill up the following 3 columns, “<strong>Price
                                Purchase</strong>”, “<strong>Share Held</strong>” and “<strong>Price Sold</strong>”
                        </td>
                    </tr>
                    <tr align="left">
                        <td colspan="3" class="font_calculator"><br>Profit & Loss Calculation</td>
                    </tr>
                    <tr align="left">
                        <td width="35%">Price Purchased (THB)</td>
                        <td width="25%"><input type="text" name="price_purchase_enter" id="price_purchase_enter"
                                onKeyPress="return BannerKey(this);" class="cellinput"></td>
                        <td valign="bottom">
                            <div id='message_price_purchase' class="noMessageError" style="display:none">Please input
                                only number.</div>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>Share Held</td>
                        <td><input type="text" name="shares_held_enter" id="shares_held_enter"
                                onKeyPress="return BannerKey(this);" class="cellinput"></td>
                        <td valign="bottom">
                            <div id='message_shares_held' class="noMessageError" style="display:none">Please input only
                                number.</div>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>Commission (%)</td>
                        <td><input type="text" name="commission_enter" id="commission_enter" value="0.25"
                                onKeyPress="return BannerKey(this);" class="cellinput"></td>
                        <td valign="bottom">
                            <div id='message_commission' class="noMessageError" style="display:none">Please input only
                                number.</div>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>Minimum Commission (THB)</td>
                        <td><input type="text" name="min_commission_enter" id="min_commission_enter" value="50"
                                onKeyPress="return BannerKey(this);" class="cellinput"></td>
                        <td valign="bottom">
                            <div id='message_min_commission' class="noMessageError" style="display:none">Please input
                                only number.</div>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>VAT (%)</td>
                        <td><input type="text" name="vat_enter" id="vat_enter" value="7"
                                onKeyPress="return BannerKey(this);" class="cellinput"></td>
                        <td valign="bottom">
                            <div id='message_vat' class="noMessageError" style="display:none">Please input only number.
                            </div>
                        </td>
                    </tr>
                    <tr align="left">
                        <td>Price Sold (THB)</td>
                        <td><input type="text" name="price_sold_enter" id="price_sold_enter" value=""
                                onKeyPress="return BannerKey(this);" class="cellinput"></td>
                        <td valign="bottom">
                            <div id='message_price_sold' class="noMessageError" style="display:none">Please input only
                                number.</div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div class="ir_textDivider"></div>
                        </td>
                    </tr>
                    <tr align="left">
                        <td colspan="3" class="font_calculator"><br>Gross Dividend Per Share</td>
                    </tr>
                    <tr align="left">
                        <td width="35%" align="left">Taxed (THB)</td>
                        <td width="25%"><input type="text" name="dividend_taxed_enter" id="dividend_taxed_enter"
                                onKeyPress="return BannerKey(this);" class="cellinput"></td>
                        <td valign="bottom">
                            <div id='message_dividend_taxed' class="noMessageError" style="display:none">Please input
                                only number.</div>
                        </td>
                    </tr>
                    <tr align="left">
                        <td align="left">Tax Exempt (THB)</td></br>
                        <td><input type="text" name="dividend_tax_exempt_enter" id="dividend_tax_exempt_enter"
                                onKeyPress="return BannerKey(this);" class="cellinput"></td>
                        <td valign="bottom">
                            <div id='message_dividend_tax_exempt' class="noMessageError" style="display:none">Please
                                input only number.</div>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td colspan=2 align="left"><br><input class="style_button" type="button" value="Calculate"
                                onClick="javascript:CheckTextInput();"></td>
                        </br>
                    </tr>
                </table>
                <tr>
                    <td align="center" valign="top"><br>
                        <table width="100%" cellspacing="1" cellpadding="3" border="0" class="webcasts">
                            <tr class="row2">
                                <td align="left">Share Held</td>
                                <td width="30%" align="left"><input type="text" name="shares_held"
                                        onFocus="document.calculator.shares_held_enter.select();"
                                        class="ir_investmentCalculator"></td>
                            </tr>
                            <tr class="row2">
                                <td align="left">Price Purchased Per Share (THB)</td>
                                <td align="left"><input type="text" name="price_purchase"
                                        onFocus="document.calculator.price_purchase_enter.select();"
                                        class="ir_investmentCalculator"></td>
                            </tr>
                            <tr class="row2">
                                <td align="left">Price Sold (THB)</td>
                                <td align="left"><input type="text" name="price_sold"
                                        onFocus="document.calculator.price_sold_enter.select();"
                                        class="ir_investmentCalculator"></td>
                            </tr>
                            <tr class="row2">
                                <td align="left">Total Gross Profit(Loss) On These Shares (THB)</td>
                                <td align="left"><input type="text" name="profit"
                                        onFocus="document.calculator.shares_held_enter.select();"
                                        class="ir_investmentCalculator"></td>
                            </tr>
                            <tr class="row2">
                                <td align="left">Less Buying And Selling Commission (THB)</td>
                                <td align="left"><input type="text" name="commission"
                                        onFocus="document.calculator.commission_enter.select();"
                                        class="ir_investmentCalculator"></td>
                            </tr>
                            <tr class="row2">
                                <td align="left">Less VAT (THB)</td>
                                <td align="left"><input type="text" name="vat"
                                        onFocus="document.calculator.vat_enter.select();"
                                        class="ir_investmentCalculator"></td>
                            </tr>
                            <tr class="row2">
                                <td align="left">Net Profit(Loss) (THB)</td>
                                <td align="left"><input type="text" name="net_profit"
                                        onFocus="document.calculator.shares_held_enter.select();"
                                        class="ir_investmentCalculator"></td>
                            </tr>
                            <tr class="row2">
                                <td align="left">As A Percentage, Your Investment Has Changed (%)</td>
                                <td align="left"><input type="text" name="invest_change_percent"
                                        onFocus="document.calculator.shares_held_enter.select();"
                                        class="ir_investmentCalculator"></td>
                            </tr>
                        </table>
                <tr>
                    <td align="center" valign="top"><br>
                        <table width="100%" cellspacing="3" cellpadding="3" border="0" class="ir_table ir_tableBorder">
                            <tr class="row1">
                                <td class="styleTotal" align="left">Net Dividend (THB)</td>
                                <td width="30%" class="style39" align="left"><input type="text" name="net_dividend"
                                        onFocus="document.calculator.dividend_taxed_enter.select();"
                                        class="ir_investmentCalculator styleTotal"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </div>


        </form>
    </div>
</section>
<!---------------------------------- Content ---------------------------------------->