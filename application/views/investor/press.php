<!---------------------------------- Content ---------------------------------------->
<section>
    <style>
        .row .ten.columns {
            width: 82.97872%;
        }

        form {
            margin: 0;
            padding: 0;
            border: 0;
            font: inherit;
            font-size: 100%;
        }

        .webcasts table tr td {
            font-family: "Kanit", sans-serif;
            padding: 12px 15px;
            font-size: 13px;
            line-height: 20px;
        }
    </style>
    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong>Press Release</strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('NewsActivities')?></a></span><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green">Press Release</span></p>
        </div>


        <form name="frm1" METHOD="POST" ACTION="https://www.irplus.in.th/Listed/RICHY/press.asp">



            <div class="twelve columns">
                <center>
                    <br><br><br>
                    <font class="no_information">
                        No Information Now
                    </font>
                </center>
            </div>




            <!--.................................................. Page Button ..................................................--->
            <div align="center">
                <div class="paginator">
                    <div id="paging_top" class="paging" style="padding-top: 20px;font-size: 13px;">


                        <input type="hidden" id="totalpage" name="totalpage" value="1">
                        <input type="hidden" id="pageno" name="pageno" value="1">
                        <input type="hidden" id="pageno_first" name="pageno_first" value="1">
                        <input type="hidden" name="date" id="date" value="">
                        <input type="hidden" name="chkFrist" id="chkFrist" value="">
                        <script language="javascript">
                            function SelectedPageChanged(page, selectedPage) {
                                document.getElementById("pageno").value = selectedPage;
                                document.forms["frm1"].submit();
                            }

                            function SelectedPageChangedMore(page, selectedPage) {
                                document.getElementById("pageno_first").value = selectedPage;
                                document.getElementById("pageno").value = selectedPage;
                                document.forms["frm1"].submit();
                            }

                            function SelectedPageChangedNext(page, selectedPage) {
                                document.getElementById("pageno_first").value = selectedPage;
                                document.getElementById("pageno").value = selectedPage;
                                document.forms["frm1"].submit();
                            }

                            function SelectedPageChangedPrev(page, selectedPage, constPageFrist) {
                                document.getElementById("pageno_first").value = constPageFrist;
                                document.getElementById("pageno").value = selectedPage;
                                document.forms["frm1"].submit();
                            }
                        </script>
                    </div>
                </div>
            </div>
            <br>
            <br><br>

        </form>
    </div>
</section>
<!---------------------------------- Content ---------------------------------------->