<!---------------------------------- Content ---------------------------------------->
<section>


    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('StockQuote')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('home')?></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('StockInformation')?></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('StockQuote')?></span></p>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ir-font padbot90">
            <div class="row">

                <div class="col-lg-6 col-md-12">
                    <?php
                    $stock_price = $this->db->get('stock_price');
                    foreach ($stock_price->result_array() as $value) {
                    ?>
                    <div class="row pad-main-left-stock-quote">
                        <div>
                            <div class="col-lg-12 col-md-12 pad-stock-quote frame-left-stock">
                                <div>
                                    <div class="text-center"> <img src="<?=base_url('uploads/stock_price/'.$value['logo']);?>"> <br>
                                        <br>
                                        <hr class="hr-half" style="margin: 0 auto;">
                                    </div>
                                    <div class="padtop50 text-sub-stock-quote">
                                        <?=$value['detail_'.$this->lang->lang()];?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12 pad-stock-quote-bottom frame-right-stock-quote">
                                <?=$value['detail2_'.$this->lang->lang()];?>
                            </div>

                        </div>
                    </div>
                    <?php } ?>
                </div>

                <div class="col-lg-5 col-md-12">
                    <div class="row pad-main-right-stock-quote">
                        <div class="stockprice-wrap-quote" style="margin: 0;">
                            <div class="display-stock-quote">
                                <div class="frame-stock-set-quote">
                                    <div class="margin-stock"> <iframe src="https://www.richy.co.th/stock_quote.html"
                                            frameborder=0 scrolling=no width="210" height="260" framepadding=0
                                            framespacing=0></iframe></div>
                                    <div>
                                        <p class="text-stock-set">Stock Price</p>
                                    </div>
                                </div>
                                <div class="frame-stock-set-quote">
                                    <div class="margin-stock"><iframe src="https://www.richy.co.th/stock_graph.html"
                                            frameborder=0 scrolling=no width="210" height="260" framepadding=0
                                            framespacing=0></iframe> </div>
                                    <div>
                                        <p class="text-stock-set">SET Index</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</section>
<!---------------------------------- Content ---------------------------------------->