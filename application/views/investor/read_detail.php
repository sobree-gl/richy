<!DOCTYPE html>
<html lang="en" class="">


<!--include file = "../../check_status_of_listed.asp" -->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Webcasts & Presentation</title>
    <meta name="keywords" content="Richy,living ">
    <meta name="description" content="Richy in living">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" href="https://www.richy.co.th/favicon.ico" type="image/x-icon">

    <!--Accordion-->
    <link rel="stylesheet" href="<?=base_url();?>css/read_detail.css">

	<!--ir-style-->
	<link rel="stylesheet" href="<?=base_url();?>css/bootstrap.css">
	<link rel="stylesheet" href="<?=base_url();?>css/ir-style.css">
	<link rel="stylesheet" href="<?=base_url();?>css/ir-style-old.css">
	<link rel="stylesheet" href="<?=base_url();?>css/table.css">

	<!--Accordion-->
	<link rel="stylesheet" href="<?=base_url();?>css/smk-accordion.css">

	<!--table-slide	-->
	<link rel="stylesheet" href="<?=base_url();?>css/scroll-hint.css">


    <!--dividend-->
    <script src="<?=base_url();?>scripts/dividend-th.js"></script>


	<link rel="stylesheet" href="<?=base_url();?>css/app1.css">
	<link rel="stylesheet" href="<?=base_url();?>css/animate.min.css">
	<link rel="stylesheet" href="<?=base_url();?>css/all.css">
	<link rel="stylesheet" href="<?=base_url();?>css/lightgallery.min.css">
	<link rel="stylesheet" href="<?=base_url();?>css/lightslider.min.css">
	<link rel="stylesheet" href="<?=base_url();?>css/style.css">

</head>


<body>

    <div class="pad-frame-read-detail">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="frame-read-detail">
                <img class="img-read-detail" src="<?=base_url();?>images/logo.svg">
            </div>
        </div>

        <?php
        $this->db->where('id', $this->input->get('param'));
        $webcasts = $this->db->get( $this->input->get('topic') );
        foreach ($webcasts->result_array() as $key => $value) {
        ?>

        <div class="col-lg-12 col-md-12 col-sm-12 wow fadeInDown" data-wow-duration="1000ms" data-wow-delay="0.3s">
            <h1 class="page__title page__title--padding page__title--gigantic"><?=$value['title_'.$this->lang->lang()];?></h1>
        </div>

        <!-- .................................................. Content ................................................  -->
        <div class="webcasts">
            <table cellSpacing=0 cellPadding=0 width="100%" border=0 align="center">
                <TR>
                    <TD>
                        <br>
                <TR>
                    <TD align="left" valign="top"> <br>
                        <?php if ($value['file']) { ?>
                        <IFRAME id="detail" src="<?=base_url('uploads/'.$this->input->get('topic').'/'.$value['file']);?>" frameBorder="0" width="100%"
                            height="1100" scrolling="yes"></IFRAME>
                        <?php } ?>
                    </TD>
                </TR>
            </table>
        </div>

        <!-- .................................................. End  Content ................................................  -->
        <?php } ?>



    </div>
    </div>


    <script>
        $(function () {
            $('a[href*="#"]:not([href="#"])').click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location
                    .hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000);
                        return false;
                    }
                }
            });
        });
    </script>

</body>

</html>