<!---------------------------------- Content ---------------------------------------->
<section>
    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;font-size: 3rem !important;"><strong>Financial
                    Statement</strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span>Financial Info</span><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green">Financial Statement</span></p>
        </div>

        <!-------------------------------------Content--------------------------------------------------->
        <div class="container">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <table width="100%" border="0" cellpadding="3" cellspacing="2"
                            style="border: 1px solid #bebebe;">
                            <tr>
                                <td align="left" valign="top">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="3">
                                        <!--  ***************  financial ************ -->
                                        <!-- ty=0 show "No Dividend"  ty=1 show "-" ty=2 no have dot ty=3 check number -->


                                        <tr valign="top" align="middle">
                                            <td width="100%">
                                                <form
                                                    action='https://www.irplus.in.th/Listed/RICHY/finance_statement.asp'
                                                    method='post' name="fsecurity">
                                                    <input type=hidden name=target value='finance_statement'>
                                                    <input type=hidden name=page_ref value='finance_statement'>

                                                    &nbsp;&nbsp;Year :
                                                    <select name="finance_year">
                                                        <option value='2019' SELECTED>2019</option>
                                                        <option value='2018'>2018</option>
                                                        <option value='2017'>2017</option>
                                                        <option value='2016'>2016</option>
                                                        <option value='2015'>2015</option>
                                                    </select>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Quarter :
                                                    <select name="finance_qtr" style="cursor:hand">
                                                        <option value="1">Quarter
                                                            1</option>
                                                        <option value="2" SELECTED>Quarter
                                                            2</option>
                                                        <option value="3">Quarter
                                                            3</option>
                                                        <option value="9">Yearly </option>
                                                    </select>

                                                    Type :
                                                    <select name="finance_type">
                                                        <option value="U" SELECTED>Company </option>

                                                        <option value="C">Consolidated</option>

                                                    </select>
                                                    <input type="submit" value="Go!" name="submit" class="style_button">
                                                    <input type=hidden name=com_id2 value=1187>
                                                    <input type="hidden" name="type_fin" value="">
                                                    <input type="hidden" name="sby" value="">
                                            </td>
                                        </tr>
                                        </form>


                                        <form name="frmselect">
                                            <tr>
                                                <td> <BR>
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td width="65%" valign="top">

                                                                <font class="fi_active">Statement of financial position
                                                                </font>
                                                                |

                                                                <a class="web"
                                                                    href="javascript:document.forms['frmselect'].elements['type_fin'].value='income';document.forms['frmselect'].submit();">Income
                                                                    Statement</a>
                                                                |

                                                                <a class="web"
                                                                    href="javascript:document.forms['frmselect'].elements['type_fin'].value='other';document.forms['frmselect'].submit();"><br>Other
                                                                    comprehensive income statement</a>
                                                                |

                                                                <a class="web"
                                                                    href="javascript:document.forms['frmselect'].elements['type_fin'].value='cash';document.forms['frmselect'].submit();"><br>
                                                                    Statement of cash flows</a>
                                                                |
                                                                <a class="web" href="download_financial.html"
                                                                    target="_blank">Download Financial Statements</a>
                                                            </td>
                                                            <td valign="top">
                                                                <table width="100%" border="0" cellpadding="0"
                                                                    cellspacing="0">
                                                                    <tr>
                                                                        <td width="20%" valign="top">% by </td>
                                                                        <td width="80%" valign="top"><input type="radio"
                                                                                name="sby" value="1" CHECKED
                                                                                onClick="javascript:document.forms['frmselect'].submit();"
                                                                                style="cursor:hand"> Common Size </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="top"> </td>
                                                                        <td valign="top"><input type="radio" name="sby"
                                                                                value="2"
                                                                                onClick="javascript:document.forms['frmselect'].submit();"
                                                                                style="cursor:hand"> Change
                                                                            <input type="hidden" name="type_fin"
                                                                                value="">
                                                                            <input type="hidden" name="finance_year"
                                                                                value="">
                                                                            <input type="hidden" name="finance_qtr"
                                                                                value="">
                                                                            <input type="hidden" name="finance_type"
                                                                                value=""></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </form>



                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="space">&nbsp;</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" leftmargin=0 cellspacing="0" cellpadding="0" border=0>
                            <tr>
                                <td>

                                    <IFRAME id="ifrSample1"
                                        src="https://www.efinancethai.com/stockfocus/IC_financial.aspx?com_id=1187&amp;lang=EN&amp;years=2019&amp;quarter=2&amp;typeid=U&amp;arrang=1&amp;type_fin=balance&amp;share=RICHY"
                                        frameBorder="0" width="100%" height="1100" scrolling="Auto"></IFRAME>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div class="row process">
                <div class=" font_sub" style="padding-bottom:1px;"></div>
            </div>
        </div>
        <!-------------------------------------Content--------------------------------------------------->
        <br />

    </div>

</section>
<!---------------------------------- Content ---------------------------------------->