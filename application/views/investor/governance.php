<!---------------------------------- Content ---------------------------------------->
<section>

    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('CorporateGovernance')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('home')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('CorporateGovernance')?></span></p>
        </div>
        <?php
        $governance = $this->db->get('governance');
        foreach ($governance->result_array() as $key => $value) {
        ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot90">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align: center;">
                    <img src="<?=base_url('uploads/governance/'.$value['image']);?>">
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 pad-text-governance">
                    <h3 class="font-gray-smoke"><?=$value['title_'.$this->lang->lang()];?></h3>
                    <hr class="hr-half">
                    <?=$value['detail_'.$this->lang->lang()];?>
                    <br>
                    <?php if (is_file('uploads/governance/'.$value['file'])) { ?>
                    <a href="<?=base_url('uploads/governance/'.$value['file']);?>" target="_blank">
                        <button class="button main expanded" type="submit"
                            style="width: auto; display: unset;">Corporate Governance Policy</button>
                    </a>
                    <?php } ?>

                </div>
            </div>
        </div>
        <?php } ?>

    </div>

</section>
<!---------------------------------- Content ---------------------------------------->