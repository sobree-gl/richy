<style>
    .table-data table tr td {
        padding: 20px !important;
    }

    table td {
        white-space: normal !important;
    }
</style>
<!---------------------------------- Content ---------------------------------------->
<section>

    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('IRCalendar')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu"
                        href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span
                    class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('NewsActivities')?></a></span><span
                    class="font-gray-smoke" style="padding: 0 1%;">/</span><span
                    class="font-mint-green"><?php echo lang('IRCalendar')?></span></p>
        </div>
        <br />
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot90">
            <div class="row">

                <div class="table-data">
                    <form name="frm1" METHOD="POST" ACTION="https://www.irplus.in.th/Listed/RICHY/ir_calendar.asp">
                        <table width="100%">
                            <thead>
                                <tr>
                                    <th width="11%">Date &amp; Time </th>
                                    <th>Event</th>
                                    <th>Location</th>
                                    <th>Relate Info</th>
                                    <th>IR Multimedia</th>
                                </tr>
                            </thead>
                            <?php
                            $calendar = $this->db->get('calendar');
                            foreach ($calendar->result_array() as $value) {
                            ?>
                            <tr align="left" valign="top">

                                <td valign="top" class="ic_font_date">
                                    <?=$value['date'];?>
                                </td>
                                <td valign="top"><?=$value['title_'.$this->lang->lang()];?></td>

                                <td valign="top"><?=$value['place_'.$this->lang->lang()];?></td>

                                <td valign="top">
                                <?php if ($value['file']) { ?>
                                    <a class="web" href="<?=site_url('investor/read_detail/?topic=calendar&param='.$value['id']);?>"
                                        target="_blank"><?php echo lang('Presentation')?></a>
                                <?php } ?>

                                </td>
                                <!-- ......................................................................  START ADD COLUMN IR MULTIMEDIA BY BOWL ...................................................................... -->
                                <td valign="middle" align="center">
                                <?php if ($value['vdo']) { ?>
                                    <a class="web" href="<?=site_url('investor/read_player/?topic=calendar&param='.$value['id']);?>"
                                        target="_blank"><?php echo lang('Presentation')?></a>
                                <?php } ?>
                                </td>
                                <!-- ......................................................................  END ADD COLUMN IR MULTIMEDIA BY BOWL ...................................................................... -->
                            </tr>
                            <?php } ?>

                        </table>
                        <br>

                        <!--.................................................. Page Button ..................................................--->
                        <div align="center">
                            <div class="paginator">
                                <div id="paging_top" class="paging" style="padding-top: 20px;font-size: 13px;">


                                    <input type="hidden" id="totalpage" name="totalpage" value="1">
                                    <input type="hidden" id="pageno" name="pageno" value="1">
                                    <input type="hidden" id="pageno_first" name="pageno_first" value="">
                                    <input type="hidden" name="date" id="date" value="">
                                    <input type="hidden" name="chkFrist" id="chkFrist" value="">
                                    <script language="javascript">
                                        function SelectedPageChanged(page, selectedPage) {
                                            document.getElementById("pageno").value = selectedPage;
                                            document.forms["frm1"].submit();
                                        }

                                        function SelectedPageChangedMore(page, selectedPage) {
                                            document.getElementById("pageno_first").value = selectedPage;
                                            document.getElementById("pageno").value = selectedPage;
                                            document.forms["frm1"].submit();
                                        }

                                        function SelectedPageChangedNext(page, selectedPage) {
                                            document.getElementById("pageno_first").value = selectedPage;
                                            document.getElementById("pageno").value = selectedPage;
                                            document.forms["frm1"].submit();
                                        }

                                        function SelectedPageChangedPrev(page, selectedPage, constPageFrist) {
                                            document.getElementById("pageno_first").value = constPageFrist;
                                            document.getElementById("pageno").value = selectedPage;
                                            document.forms["frm1"].submit();
                                        }
                                    </script>
                                </div>
                            </div>
                        </div>
                        <br>
                    </form>
                </div>


            </div>
        </div>


    </div>

</section>
<!---------------------------------- Content ---------------------------------------->