<!---------------------------------- Content ---------------------------------------->
<section>
    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong>Annual Report</strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span>Financial Info</span><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green">Annual Report</span></p>
        </div>
        <div class="row">
            <form name="frm1" method="post" action="https://www.irplus.in.th/Listed/RICHY/annual_report.asp">

                <div class="row">
                    <div class="col-lg-5">
                        <img src="<?=base_url();?>images/an_richy_2018_E.gif" style="min-height: 380px;">
                    </div>
                    <div class="col-lg-7">

                        <br><br>
                        <p><strong>File :</strong> Annual Report 2018&nbsp;(Latest)
                            <img src="<?=base_url();?>images/icon_06.gif" width="24" height="11" alt="" />
                            <br><strong>Date :</strong> 31 May 2019
                            <br><strong>Type :</strong> PDF File
                            <!--<br><strong>Size :</strong> MB-->
                            <br><br>
                            <p class="text_09">Note</p>
                            <p>To Download the File, Your Computer Must <br>Have "Adobe Acrobat" to Read Files "PDF"</p>
                            <br>
                            <a href="annual/an_richy_2018_E.pdf" target="_bank">
                                <span class="btn-1">Download</span>
                            </a>



                    </div>
                </div><!-- / row -->

                <div class="row">

                    <div class="col-lg-6">
                        <a href="annual/an_richy_2017_E.pdf" target="_bank">
                            <div class="list-wrap">
                                <div class="cover">
                                    <img src="<?=base_url();?>images/an_richy_2017_E.gif">
                                </div>
                                <p style="margin-top: 8px;"><strong>File :</strong> Annual Report 2017
                                    <br><strong>Date :</strong> 24 May 2018
                                    <br><strong>Type :</strong> PDF File
                                    <!--<br><strong>Size :</strong> MB-->
                                    <br><br>
                            </div><!-- /list-wrap -->
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="annual/an_richy_2016_E.pdf" target="_bank">
                            <div class="list-wrap">
                                <div class="cover">
                                    <img src="<?=base_url();?>images/an_richy_2016_E.gif">
                                </div>
                                <p style="margin-top: 8px;"><strong>File :</strong> Annual Report 2016
                                    <br><strong>Date :</strong> 28 Mar 2017
                                    <br><strong>Type :</strong> PDF File
                                    <!--<br><strong>Size :</strong> MB-->
                                    <br><br>
                            </div><!-- /list-wrap -->
                        </a>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-6">
                        <a href="annual/an_richy_2015_E.pdf" target="_bank">
                            <div class="list-wrap">
                                <div class="cover">
                                    <img src="<?=base_url();?>images/an_richy_2015_E.gif">
                                </div>
                                <p style="margin-top: 8px;"><strong>File :</strong> Annual Report 2015
                                    <br><strong>Date :</strong> 25 Mar 2016
                                    <br><strong>Type :</strong> PDF File
                                    <!--<br><strong>Size :</strong> MB-->
                                    <br><br>
                            </div><!-- /list-wrap -->
                        </a>
                    </div>

                    <div class="col-lg-6">
                        <a href="annual/an_richy_2014_E.pdf" target="_bank">
                            <div class="list-wrap">
                                <div class="cover">
                                    <img src="<?=base_url();?>images/an_richy_2014_E.gif">
                                </div>
                                <p style="margin-top: 8px;"><strong>File :</strong> Annual Report 2014
                                    <br><strong>Date :</strong> 08 May 2015
                                    <br><strong>Type :</strong> PDF File
                                    <!--<br><strong>Size :</strong> MB-->
                                    <br><br>
                            </div><!-- /list-wrap -->
                        </a>
                    </div>

                </div>


                <!--.................................................. Page Button ..................................................--->
                <div align="center">
                    <div class="paginator">
                        <div id="paging_top" class="paging" style="padding-top: 20px;font-size: 13px;">


                            <input type="hidden" id="totalpage" name="totalpage" value="1">
                            <input type="hidden" id="pageno" name="pageno" value="1">
                            <input type="hidden" id="pageno_first" name="pageno_first" value="">
                            <input type="hidden" name="date" id="date" value="">
                            <input type="hidden" name="chkFrist" id="chkFrist" value="">
                            <script language="javascript">
                                function SelectedPageChanged(page, selectedPage) {
                                    document.getElementById("pageno").value = selectedPage;
                                    document.forms["frm1"].submit();
                                }

                                function SelectedPageChangedMore(page, selectedPage) {
                                    document.getElementById("pageno_first").value = selectedPage;
                                    document.getElementById("pageno").value = selectedPage;
                                    document.forms["frm1"].submit();
                                }

                                function SelectedPageChangedNext(page, selectedPage) {
                                    document.getElementById("pageno_first").value = selectedPage;
                                    document.getElementById("pageno").value = selectedPage;
                                    document.forms["frm1"].submit();
                                }

                                function SelectedPageChangedPrev(page, selectedPage, constPageFrist) {
                                    document.getElementById("pageno_first").value = constPageFrist;
                                    document.getElementById("pageno").value = selectedPage;
                                    document.forms["frm1"].submit();
                                }
                            </script>
                        </div>
                    </div>
                </div>
                <br>

            </form>
        </div>
    </div>
</section>
<!---------------------------------- Content ---------------------------------------->