<style>
    .table-data table tr td {
        padding: 20px !important;
    }
</style>

<section>

    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('MajorShareholder')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="home.asp"><?php echo lang('IRMenu')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('ShareholderInfo')?></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('MajorShareholder')?></span></p>
        </div>
        <div class="col-lg-12">
            <div class="row">


                <div class="row">
                    <!-- <p>Major Shareholder as of 2019-05-03</p><br><br> -->

                    <div class="table-data">

                        <table cellspacing="0" width="100%">

                            <tbody>
                                <tr>
                                    <th width="9%" align="center">ลำดับ</th>
                                    <th width="22%" align="center">รายชื่อผู้ถือหุ้น</th>
                                    <th colspan="61%">ภายหลังเสนอขายหุ้นต่อ</th>
                                </tr>
                                <tr class="highlight">
                                    <td width="9%" align="center">&nbsp;</td>
                                    <td width="22%" align="left">&nbsp;</td>
                                    <td width="46%" align="right">จำนวน (หุ้น)</td>
                                    <td width="15%" align="right">สัดส่วน</td>
                                </tr>

                                <?php
                                $key = 1;
                                $major = $this->db->get('major');
                                foreach ($major->result_array() as $value) {
                                ?>
                                <tr align="left" valign="top" class="tabledetail">
                                    <td align="center"><?=$key;?></td>
                                    <td style="text-align: left;">
                                        <?=$value['title_'.$this->lang->lang()];?>
                                    </td>
                                    <td align="right"><?=$value['total'];?></td>
                                    <td align="right"><?=$value['proportion'];?></td>
                                </tr>
                                <?php $key++; } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <?php
        $major_info = $this->db->get('major_info');
        foreach ($major_info->result_array() as $value1) {
            echo $value1['detail_'.$this->lang->lang()];
        }
        ?>
        <br><br><br><br>

    </div>
</section>