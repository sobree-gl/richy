<!---------------------------------- Content ---------------------------------------->
<section>


    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('VisionMission')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('home')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('CorporateInfo')?></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('VisionMission')?></span></p>
        </div>


        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot90">
            <div class="row">

            <?php
            $passion = $this->db->get('passion');
            foreach ($passion->result_array() as $key => $value) {
            ?>
                <?php if ($key==0) { ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="vision-wrap">
                        <div class="vision-img" style="background: url('<?=base_url('uploads/passion/'.$value['image']);?>') no-repeat;"></div>
                        <div class="vision-detail">
                            <p class="text_11"><?=$value['title_'.$this->lang->lang()];?></p>
                            <?=$value['detail_'.$this->lang->lang()];?>
                        </div><!-- /vision-detail -->
                    </div><!-- /vision-wrap -->
                </div>
                <?php } ?>

                <?php if ($key==1) { ?>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="mission-wrap">
                        <div class="mission-img" style="background: url('<?=base_url('uploads/passion/'.$value['image']);?>') no-repeat;"></div>
                        <div class="mission-detail liststyle">
                            <p class="text_11"><?=$value['title_'.$this->lang->lang()];?></p>
                            <?=$value['detail_'.$this->lang->lang()];?>
                        </div><!-- /mission-detail -->
                    </div><!-- /mission-wrap -->
                </div>
                <?php } ?>

                <?php if ($key==2) { ?>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="value-wrap">
                        <div class="value-img" style="background: url('<?=base_url('uploads/passion/'.$value['image']);?>') no-repeat;"></div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <p class="text_11"><?=$value['title_'.$this->lang->lang()];?></p>
                            </div>

                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 liststyle2">
                                <div class="value-detail">
                                    <?=$value['detail_'.$this->lang->lang()];?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>

                <?php } ?>
            </div>
        </div>
    </div>

</section>
<!---------------------------------- Content ---------------------------------------->