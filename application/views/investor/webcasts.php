<style>
    .table-data table tr td {
        padding: 20px !important;
    }
</style>
<!---------------------------------- Content ---------------------------------------->
<section>
    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('webcasts')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('home')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('webcasts')?></span></p>
        </div>
        <div class="table-data">
            <table class="table table-condensed">
                <thead>
                    <tr>
                        <th>Legend:</th>
                        <th><img src="<?=base_url();?>images/pic_present.gif" width="23" height="12">Presentation (pdf)
                        </th>
                        <th><img src="<?=base_url();?>images/pic_transcripts.gif" width="23" height="12">Transcripts
                            (pdf)</th>
                        <th><img src="<?=base_url();?>images/pic_webcast.gif" width="23" height="12">Webcast </th>
                        <th><img src="<?=base_url();?>images/pic_audio.gif" width="23" height="12"> Audio </th>
                    </tr>
                </thead>
            </table>

            <table class="table table-condensed">

                <thead>
                    <tr>
                        <th colspan="6">Current Event</th>
                    </tr>
                </thead>

                <?php
                $webcasts = $this->db->get('webcasts');
                foreach ($webcasts->result_array() as $key => $value) {
                ?>
                <tr class="wp_color4">

                    <td valign="middle"><?=$value['title_'.$this->lang->lang()];?> </td>


                    <td align="center" valign="middle">
                        <?php if ($value['file']) { ?>
                        <a class="web" href="<?=site_url('investor/read_detail/?topic=webcasts&param='.$value['id']);?>" target="_blank">
                            <img src="<?=base_url();?>images/pic_present.gif" border="0" width="23" height="12">
                        </a>
                        <?php } ?>
                    </td>

                    <td align="center" valign="middle">&nbsp;</td>

                    <td align="center" valign="middle">
                        <?php if ($value['vdo']) { ?>
                        <a class="web" href="<?=site_url('investor/read_player/?topic=webcasts&param='.$value['id']);?>" target="_blank">
                            <img src="<?=base_url();?>images/pic_webcast.gif" width="23" height="12" border="0" style="cursor:hand;">
                        </a>
                        <?php } ?>
                    </td>

                    <td align="center" valign="middle">&nbsp;</td>

                    <td align="left" valign="middle"><span class="font_datetime">28.11.2018</span></td>
                </tr>
                <?php } ?>

            </table>
            <br>

            <!--.................................................. Page Button ..................................................--->
            <div align="center">
                <div class="paginator">
                    <div id="paging_top" class="paging" style="padding-top: 20px;font-size: 13px;">


                        <input type="hidden" id="totalpage" name="totalpage" value="1">
                        <input type="hidden" id="pageno" name="pageno" value="1">
                        <input type="hidden" id="pageno_first" name="pageno_first" value="">
                        <input type="hidden" name="date" id="date" value="">
                        <input type="hidden" name="chkFrist" id="chkFrist" value="">
                        <script language="javascript">
                            function SelectedPageChanged(page, selectedPage) {
                                document.getElementById("pageno").value = selectedPage;
                                document.forms["frm1"].submit();
                            }

                            function SelectedPageChangedMore(page, selectedPage) {
                                document.getElementById("pageno_first").value = selectedPage;
                                document.getElementById("pageno").value = selectedPage;
                                document.forms["frm1"].submit();
                            }

                            function SelectedPageChangedNext(page, selectedPage) {
                                document.getElementById("pageno_first").value = selectedPage;
                                document.getElementById("pageno").value = selectedPage;
                                document.forms["frm1"].submit();
                            }

                            function SelectedPageChangedPrev(page, selectedPage, constPageFrist) {
                                document.getElementById("pageno_first").value = constPageFrist;
                                document.getElementById("pageno").value = selectedPage;
                                document.forms["frm1"].submit();
                            }
                        </script>
                    </div>
                </div>
            </div>
            <br>
            </form>
        </div>
    </div>
</section>
<!---------------------------------- Content ---------------------------------------->