<!---------------------------------- Content ---------------------------------------->
<section>
    <style>
        table th {
            background-color: #1d9f68;
        }
    </style>
    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong>Research</strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green">Research</span></p>
        </div>
        <form name="frm1" method="post" action="https://www.irplus.in.th/Listed/RICHY/analyst.asp">
            <p>
                <table cellspacing='0' width="100%">
                    <tr align="center" valign="middle">
                        <th width="20%">Date</th>
                        <th width="55%">Title</th>
                        <th>Analyst</th>
                    </tr>

                    <tr>
                        <td valign="top" colspan="3" align="center" class="bgcolor_no_information">
                            <div align="center">
                                <font class="no_information">
                                    No Information Now
                                </font>
                            </div>
                        </td>
                    </tr>

                </table>
            </p>
            <!--+++++++++++++++++++++++++ Page Button+++++++++++++++++++++++--->
            <div align="center">
                <br>
                <div align="center">
                    <div class="paginator">
                        <div id="paging_top" class="paging" style="padding-top: 20px;font-size: 13px;">


                            <input type="hidden" id="totalpage" name="totalpage" value="1">
                            <input type="hidden" id="pageno" name="pageno" value="1">
                            <input type="hidden" id="pageno_first" name="pageno_first" value="">
                            <input type="hidden" name="date" id="date" value="">
                            <input type="hidden" name="chkFrist" id="chkFrist" value="">
                            <script language="javascript">
                                function SelectedPageChanged(page, selectedPage) {
                                    document.getElementById("pageno").value = selectedPage;
                                    document.forms["frm1"].submit();
                                }

                                function SelectedPageChangedMore(page, selectedPage) {
                                    document.getElementById("pageno_first").value = selectedPage;
                                    document.getElementById("pageno").value = selectedPage;
                                    document.forms["frm1"].submit();
                                }

                                function SelectedPageChangedNext(page, selectedPage) {
                                    document.getElementById("pageno_first").value = selectedPage;
                                    document.getElementById("pageno").value = selectedPage;
                                    document.forms["frm1"].submit();
                                }

                                function SelectedPageChangedPrev(page, selectedPage, constPageFrist) {
                                    document.getElementById("pageno_first").value = constPageFrist;
                                    document.getElementById("pageno").value = selectedPage;
                                    document.forms["frm1"].submit();
                                }
                            </script>
                        </div>
                    </div>
                </div>
                <br>
            </div>
            <p class="font_disclaim">Disclaimer</p>
            <table width="100%" cellspacing="0">
                <tr>
                    <th>
                        <br>
                        <font style="font-weight:normal; color:#555555; font-size:14px;">

                            The information in this website was created for the purposing of providing information that
                            will help investors make informed decisions.
                            It was not created to solicit investors to buy or sell any securities. The final decision
                            and responsibility for investments rests solely with the user of this website and its
                            content.
                            Please also be aware that information on this website may be changed, modified, added or
                            removed at any time without prior notice.
                            Although Online Asset Company Limited has made careful efforts regarding the accuracy of the
                            contents here, Online Asset Company limited assumes no responsibility for problems
                            including,
                            but not limited to, incorrect information or problems resulting from downloading of data.


                        </font>

                    </th>
                </tr>
            </table>
        </form>
    </div>
</section>
<!---------------------------------- Content ---------------------------------------->