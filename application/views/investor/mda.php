<!---------------------------------- Content ---------------------------------------->
<section>


    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong>MD &amp; A</strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu" href="<?=site_url('investor');?>"><?php echo lang('home')?></a></strong><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span>Financial Info</span><span class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span class="font-mint-green">MD &amp; A</span></p>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padbot90">
            <div class="row">
                <div class="padbot50" style="display: inline-block">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <div class="cover-current"> <img src="<?=base_url();?>images/img_11.png"> </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12"> <br>
                        <br>
                        <p class="text_08">Richy Place 2002 Co., Ltd is prominent in real estate development sector
                            emphasizing the need,
                            satisfaction, and happiness in living to the target consumers,</p>
                        <hr class="hr-half">
                        <p>which is the product pattern, site location, environment friendliness, outstanding efficient
                            service to
                            gain confidence to all parties concerned.</p>
                        <p>Richy Place 2002 Co., Ltd determines to build a society filled with happiness for the
                            residents in all areas:
                            Rich in Premier Location
                            Rich in Urban Living &amp; Life Style
                            Rich in Design
                            Rich in Construction
                            Rich in Environment
                            Perfectly meet the consumers desire and lifestyle in order to present
                            happiness to residents in all Company’s projects.</p>
                        <br>
                        <a href="pdf/MDA_Q2_19_E.pdf" target="_blank"> <span class="btn-1">MD &amp; A (Latest)</span>
                        </a>
                    </div>
                </div>
                <div class="padbot50" style="display: inline-block; width: 100%;">
                    <div class="twelve columns">
                        <div class="table-data">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <th align="center">File</th>
                                        <th align="center">Year / Quarter</th>
                                        <th align="center">Size</th>
                                        <th align="center">Download File</th>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q2 / 2019</td>
                                        <td align="center">329 kb</td>
                                        <td align="center"><a href="pdf/MDA_Q2_19_E.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q1 / 2019</td>
                                        <td align="center">274 kb</td>
                                        <td align="center"><a href="pdf/MDA_Q1_19_E.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">2018</td>
                                        <td align="center">254 kb</td>
                                        <td align="center"><a href="pdf/mda_2018_E.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q3 / 2018</td>
                                        <td align="center">325 kb</td>
                                        <td align="center"><a href="pdf/MD%26A_Q3_18_E.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q2 / 2018</td>
                                        <td align="center">96 kb</td>
                                        <td align="center"><a href="pdf/MD%26A_Q2_18_E.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q1 / 2018</td>
                                        <td align="center">269 kb</td>
                                        <td align="center"><a href="pdf/MD%26A_Q1_18_E.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">2017</td>
                                        <td align="center">132 kb</td>
                                        <td align="center"><a href="pdf/MD%26A_new_2017_E.pdf"
                                                target="_blank">Download</a></td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q3 / 2017</td>
                                        <td align="center">82 kb</td>
                                        <td align="center"><a href="pdf/MD%26A_Q3_17_E.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q2 / 2017</td>
                                        <td align="center">239 kb</td>
                                        <td align="center"><a href="pdf/MD%26A_Q2_17_E.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q1 / 2017</td>
                                        <td align="center">355 kb</td>
                                        <td align="center"><a href="pdf/MD%26A_Q1_17_E.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">2016</td>
                                        <td align="center">319 kb</td>
                                        <td align="center"><a href="pdf/MD%26A_2016_E.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q3 / 2016</td>
                                        <td align="center">344 kb</td>
                                        <td align="center"><a href="pdf/MD%26A_Q3_2559_EN.pdf"
                                                target="_blank">Download</a></td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q2 / 2016</td>
                                        <td align="center">298 kb</td>
                                        <td align="center"><a href="pdf/MD%26A_Q2_2559_EN.pdf"
                                                target="_blank">Download</a></td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q1 / 2016</td>
                                        <td align="center">314 kb</td>
                                        <td align="center"><a href="pdf/MD%26A_Q1_2559_EN.pdf"
                                                target="_blank">Download</a></td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">2015</td>
                                        <td align="center">263 kb</td>
                                        <td align="center"><a href="pdf/mda_15_en.pdf" target="_blank">Download</a></td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q3 / 2015</td>
                                        <td align="center">416 kb</td>
                                        <td align="center"><a href="pdf/mda_3q_15_en.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q2 / 2015</td>
                                        <td align="center">412 kb</td>
                                        <td align="center"><a href="pdf/mda_2q_15_en.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Q1 / 2015</td>
                                        <td align="center">341 kb</td>
                                        <td align="center"><a href="pdf/mda_1q_15_en.pdf" target="_blank">Download</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">2014</td>
                                        <td align="center">389 kb</td>
                                        <td align="center"><a href="pdf/mda_14_en.pdf" target="_blank">Download</a></td>
                                    </tr>
                                    <tr>
                                        <td align="center">Management Discussion &amp; Analysis</td>
                                        <td align="center">Filing Version</td>
                                        <td align="center">112 kb</td>
                                        <td align="center"><a href="pdf/mda_13_en.pdf" target="_blank">Download</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- / table-data -->
                    </div>
                    <!-- / twelve columns -->
                </div>
                <!-- / row -->
            </div>
        </div>
    </div>

</section>
<!---------------------------------- Content ---------------------------------------->