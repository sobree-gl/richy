
<section>
    <div class="grid-container display-main">


        <div class="frame-agm">
            <div>
                <img class="hidden-xs" src="<?=base_url();?>images/banner-agm.jpg">
                <img class="hidden-lg hidden-md hidden-sm" src="<?=base_url();?>images/banner-agm-mobile.jpg">
            </div>

            <div>
                <div class="col-lg-6 col-md-6 col-sm-6" style="background: rgb(29, 159, 104);">
                    <div class="row">
                        <div class="bg-amg-frame">
                            <p class="font-white" style="margin: 0; padding: 7px 0;"><strong>RICHY-W2</strong></p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6" style="background: rgb(29, 159, 104);">
                    <div class="row">
                        <div class="bg-amg-frame">
                            <p class="font-white" style="margin: 0; padding: 7px 0;"><strong>RICHY-W2</strong></p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12" style="background: rgb(246, 246, 246);">
                    <div class="row">

                        <?php
                        $voucher = $this->db->get('voucher');
                        foreach ($voucher->result_array() as $value) {
                            if ($value['file']) {
                                $file = base_url('uploads/voucher/'.$value['file']);
                                $target = '_blank';
                            } else {
                                $file = 'javascript:void(0)';
                                $target = '';
                            }
                        ?>

                        <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="bg-text-agm pad-agm-1">
                            <div class="border-agm">
                                <div class="wd-icon-agm">
                                    <a href="<?=$file;?>" target="<?=$target;?>" class="a-agm"><img
                                            src="<?=base_url();?>images/icon-agm.png" class="icon-agm"></a>
                                </div>
                                <div class="wd-text-agm">
                                    <a href="<?=$file;?>" target="<?=$target;?>" class="a-agm">
                                        <p><?=$value['title_'.$this->lang->lang()];?></p>
                                    </a>
                                </div>
                            </div>
                        </div>
                        </div>
                        <?php } ?>

                    </div>
                </div>

            </div>
        </div>


        <div class="news-stockprice ir-font">
            <div class="grid-container">
                <div class="row">
                    <!-- News -->
                    <div class="col-md-6">
                        <div class="news-wrap">
                            <!-- ................................................ i_news_update ........................................................... -->
                            <!-- News -->
                            <div class="set-news">
                                <h3><strong><?php echo lang('NewsUpdate')?></strong></h3>
                                <hr class="hr-half">

                                <?php
                                function DateThai($strDate)
                                {
                                    $strYear = date("Y",strtotime($strDate))+543;
                                    $strMonth= date("n",strtotime($strDate));
                                    $strDay= date("j",strtotime($strDate));
                                    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
                                    $strMonthThai=$strMonthCut[$strMonth];
                                    return "$strDay $strMonthThai $strYear";
                                }
                    
                                function DateEng($strDate)
                                {
                                    $strYear = date("Y",strtotime($strDate));
                                    $strMonth= date("n",strtotime($strDate));
                                    $strDay= date("j",strtotime($strDate));
                                    $strMonthCut = Array('','Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
                                    $strMonthThai=$strMonthCut[$strMonth];
                                    return "$strDay $strMonthThai $strYear";
                                }

                                $news_update = $this->db->get('news_update');
                                foreach ($news_update->result_array() as $value) {
                                ?>
                                <a href="<?=site_url('investor/read_frame/?topic=news_update&param='.$value['id']);?>"
                                    target=_blank>
                                    <div class="news-list">
                                        <div class="bullet-date">
                                            <p class="p-date"><strong><?=date("j",strtotime($value['date']));?><br><?=date("n",strtotime($value['date']));?></strong></p>
                                            <!--<p class="p-date"><strong>Aug</strong></p>-->
                                        </div>
                                        <?php $date_news_update = DateEng($value['date']);
                                        if($this->lang->lang()=='th'){ $date_news_update = DateThai($value['date']); } ?>
                                        <p class="date-detail"> Post : <span><?=$date_news_update;?></span> </p>
                                        <p class="content-newshot"><?=$value['title_'.$this->lang->lang()];?></p>
                                    </div>
                                </a>
                                <?php } ?>

                                <a href="<?=site_url('investor/news_update');?>" class="more-btn"> <img src="<?=base_url();?>images/more_btn%401x.png" alt=""
                                        title="ทั้งหมด"> </a>
                            </div>


                            <!-- /News -->


                            <!-- ............................................... i_news_update ........................................................ -->

                            <!-- ................................................ i_set_announcement ........................................................... -->
                            <div class="set-news" style="margin-top: 35px;">
                                <h3><strong><?php echo lang('SETAnnoucement')?></strong></h3>
                                <hr class="hr-half">

                                <?php
                                $set_announcement = $this->db->get('set_announcement');
                                foreach ($set_announcement->result_array() as $val_set) {
                                ?>

                                <a class="kanit_04"
                                    href="<?=site_url('investor/read_frame/?topic=set_announcement&param='.$val_set['id']);?>"
                                    target="_blank">
                                    <div class="news-list">
                                        <div class="bullet-date">
                                            <p class="p-date"><strong><?=date("j",strtotime($val_set['date']));?><br><?=date("n",strtotime($val_set['date']));?></strong></p>
                                            <!-- <p class="p-date"><strong>Aug</strong></p>-->
                                        </div>
                                        <?php $date_set_announcement = DateEng($val_set['date']);
                                        if($this->lang->lang()=='th'){ $date_set_announcement = DateThai($val_set['date']); } ?>
                                        <p class="date-detail"> Post : <span><?=$date_set_announcement;?></span> </p>
                                        <p class="content-newshot"> <?=$val_set['title_'.$this->lang->lang()];?></p>
                                    </div>
                                </a>
                                <?php } ?>

                                <a href="<?=site_url('investor/set_announcement');?>" class="more-btn"> <img src="<?=base_url();?>images/more_btn%401x.png"
                                        alt="" title="ทั้งหมด"> </a>
                            </div>




                            <!-- ............................................... i_set_announcement ........................................................ -->

                        </div>
                    </div>
                    <!-- Stock Price -->
                    <div class="col-md-6">
                        <div class="stockprice-wrap">
                            <div class="display-stock">
                                <div class="frame-stock-set">
                                    <div class="margin-stock"> <iframe src="https://www.richy.co.th/stock_quote_th.html"
                                            frameborder=0 scrolling=no width="210" height="260" framepadding=0
                                            framespacing=0></iframe></div>
                                    <div>
                                        <p class="text-stock-set">Stock Price</p>
                                    </div>
                                </div>
                                <div class="frame-stock-set">
                                    <div class="margin-stock"> <iframe src="https://www.richy.co.th/stock_graph.html"
                                            frameborder=0 scrolling=no width="210" height="260" framepadding=0
                                            framespacing=0></iframe> </div>
                                    <div>
                                        <p class="text-stock-set">SET Index</p>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <h1 class="hl-detail-stock font-mint-green"><strong>ข้อมูลราคาหลักทรัพย์</strong></h1>
                            <p class="sub-detail-stock">ชื่อหุ้น : RICHY</p>
                            <br>
                            <p>
                                ข้อมูลที่เปิดเผยในราคาหุ้นที่สำคัญของ บริษัท และปริมาณข้อมูล ซื้อ - ขาย
                                ความเคลื่อนไหวและการเปลี่ยนแปลงของราคาหุ้นราคาล่าสุดของ บริษัท ริชชี่เพลส 2002 จำกัด
                                (มหาชน)</p>
                            <br>
                            <div class="frame-button-stock">
                                <a href="https://www.set.or.th/set/historicaltrading.do?symbol=RICHY&amp;ssoPageId=2&amp;language=th&amp;country=TH"
                                    target="_blank">
                                    <button class="button main-border expanded" type="submit"
                                        style="width: auto; display: unset;">ราคาหลักทรัพย์ย้อนหลัง</button>
                                </a>
                                <a href="calculator.html">
                                    <button class="button main-green expanded" type="submit"
                                        style="width: auto; display: unset;">เครื่องคำนวณการลงทุน</button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ................................................ i_ir_calendar ........................................................... -->

<section>
    <div class="frame-graph">
        <div class="grid-container">
            <div class="display-graph">
                <div class="col-lg-6 col-md-6 pad-financial-all">
                    <div class="display-calendar">
                        <div>
                            <input type="hidden" class="datetime" id="tail-datetime-calendar">
                            <div id="datetime-demo-holder" class="datetime-folder"></div>
                        </div>


                    </div>
                    <br>
                    <h3 class="font-white"><strong>ปฏิทินกิจกรรม</strong></h3>
                    <p class="font-gray-smoke">ปฏิทินนักลงทุนแสดงกิจกรรมกิจกรรมที่จะเกิดขึ้นในวันต่างๆ
                        โดยนักลงทุนสามารถติดตามกิจกรรมเราได้</p>

                    <br>
                    <button class="button main-green expanded" type="submit"
                        style="width: auto; display: unset; margin: 0;"><a href="<?=site_url('investor/ir_calendar');?>"
                            class="more-btn-graph" style="color:#fff!important;">ดูทั้งหมด</a></button>
                </div>

                <div class="col-lg-6 col-md-6 pad-financial-all">
                    <div class="display-highcharts">
                        <div id="highcharts-80ba1be8-d0dc-4282-b732-d98a92f17424" class="highcharts-frame"></div>
                    </div>
                    <br>
                    <h3 class="font-white"><strong>ข้อมูลสำคัญทางการเงิน</strong></h3>
                    <p class="font-gray-smoke">นำเสนอข้อมูลและแสดงงบการเงินเป็นงบการเงินสะสมรายไตรมาส โดย บริษัท
                        ริชชี่เพลส 2002 จำกัด (มหาชน)</p>

                    <br>
                    <p class="font-white"><span class="circle-finance"></span> สรุปประจำปี / ข้อมูลล่าสุด: 2561</p>
                    <div class="set-news">
                        <a href="financial_highlight.html" class="more-btn-graph">
                            <img src="<?=base_url();?>images/more_btn%402x.png" alt="" title="ทั้งหมด">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function () {
        tail.DateTime("#tail-datetime-calendar", {
            animate: true,
            classNames: true,
            dateFormat: "YYYY-mm-dd",
            // dateStart: false,
            dateRanges: [],
            dateBlacklist: true,
            dateEnd: false,
            locale: "en",
            position: "#datetime-demo-holder",
            rtl: "auto",
            startOpen: true,
            stayOpen: true,
            timeFormat: false,
            timeHours: null,
            timeMinutes: null,
            timeSeconds: 0,
            today: true,
            tooltips: [{
                date: "2018-11-28",
                text: "<strong>บริษัทจดทะเบียนพบผู้ลงทุน สำหรับผลประกอบการไตรมาส 3/2561 เวลา 13:00 - 14:00</strong> <br> <br> ณ ห้องประชุม 603 อาคารตลาดหลักทรัพย์แห่งประเทศไทย 93 ถนนรัชดาภิเษก แขวงดินแดง เขตดินแดง กรุงเทพมหานคร",
                color: "#3067b0"
            }, ],
            viewDefault: "days",
            viewDecades: true,
            viewYears: true,
            viewMonths: true,
            viewDays: true,
            weekStart: 0
        });
    });
</script>

<!-- ............................................... i_ir_calendar ........................................................ -->

<!-- ................................................ i_general_meeting ........................................................... -->

<section>
    <div class="grid-container display-meeting">
        <div class="col-lg-6 col-md-6 col-sm-12 ir-font frame-meeting">
            <h3><strong>การประชุมสามัญผู้ถือหุ้น</strong></h3>
            <hr class="hr-half">
            <p style="margin: 0;">การประชุมสามัญประจำปี (AGM) เป็นการรวบรวมรายปีของผู้ถือหุ้นที่สนใจของ บริษัท
                ผู้ถือหุ้นที่มีสิทธิออกเสียงลงคะแนนในประเด็นปัจจุบันเช่นการแต่งตั้งคณะกรรมการ บริษัท ค่าตอบแทนผู้บริหาร
            </p>

            <div>
                <div class="crop-meeting-icon">
                    <a class="a-meeting-icon border-meeting-1" href="<?=site_url('investor/general_meeting');?>">
                        <div class="img-icon-meeting-1"></div>
                        <p class="p-meeting"><strong>การประชุมสามัญผู้ถือหุ้นของบริษัท</strong></p>
                    </a>

                    <a class="a-meeting-icon border-meeting-2" href="<?=site_url('investor/general_meeting');?>">
                        <div class="img-icon-meeting-2"></div>
                        <p class="p-meeting"><strong>มติ / นาทีประชุมผู้ถือหุ้นของบริษัท</strong></p>
                    </a>

                    <a class="a-meeting-icon border-meeting-3" href="<?=site_url('investor/general_meeting');?>">
                        <div class="img-icon-meeting-3"></div>
                        <p class="p-meeting"><strong>วาระการเสนอชื่อและคำถามล่วงหน้า</strong></p>
                    </a>

                    <a class="a-meeting-icon border-meeting-4" href="<?=site_url('investor/general_meeting');?>">
                        <div class="img-icon-meeting-4"></div>
                        <p class="p-meeting"><strong>หนังสือเชิญประชุมสามัญผู้ถือหุ้น</strong></p>
                    </a>
                </div>

                <button class="button main-green expanded" type="submit"
                    style="width: auto; display: unset; margin: 0;"><a href="<?=site_url('investor/general_meeting');?>"
                        style="color:#fff;">การประชุมสามัญ</a></button>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12 ir-font frame-img-meeting">
            <img src="<?=base_url();?>images/meeting.png">
        </div>
    </div>
</section>

<!-- ............................................... i_general_meeting ........................................................ -->


<!-- ................................................ i_annual_report ........................................................... -->


<section>
    <div class="frame-annual">
        <div class="grid-container display-annual">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 annual-group-button">
                <div class="cover-current-home">
                    <img src="<?=base_url();?>images/an_richy_2018.gif">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 annual-group">
                <div>
                    <h3 class="font-white"><strong>รายงานประจำปี RICHY</strong></h3>
                    <p class="font-gray-smoke">
                        รายงานประจำปีฉบับนี้้นำเสนอผลสำเร็จของการดำเนินงานตามแผนปฏิบัติงานของบริษัท ริชี่เพลซ 2002 จำกัด
                        (มหาชน) เพื่อให้นักลงทุนได้รับข้อมูลนโยบายและภาพรวมของการประกอบธุรกิจ ลักษณะการประกอบธุรกิจ
                        ปัจจัยความเสี่ยง ข้อมูลหลักทรัพย์ และข้อมูลอื่นๆ</p>

                    <br>
                    <div class="annual-button">
                        <a href="annual/an_richy_2018.pdf" target="_blank">
                            <button class="button main-green expanded" type="submit"
                                style="width: auto; display: unset;">ดาวน์โหลดฉบับล่าสุด</button>
                        </a>
                        <a href="annual_report.html">
                            <button class="button main-border expanded" type="submit"
                                style="width: auto; display: unset;">ฉบับอื่นๆ</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- ............................................... i_annual_report ........................................................ -->


<section>
    <div class="frame-subscribe">
        <div class="grid-container display-subscribe">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 subscribe-group">
                <h3 class="font-mint-green"><strong>รับข่าวสาร?</strong></h3>
                <h6 class="font-white">บริษัท ริชชี่เพลส 2002 จำกัด (มหาชน)</h6>
                <br>
                <h5 class="font-white"><strong>ต้องการรับการแจ้งเตือนเมื่อมีการเผยแพร่ข่าวของเราหรือไม่
                        ลงทะเบียนเพื่อเป็นคนแรกที่รู้</strong></h5>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 subscribe-group-button">
                <button class="button main-green expanded button-subscribe" type="submit"
                    style="width: auto; display: unset; margin: 0;"><a href="request_alerts.html"
                        style="color:#ffffff;">ลงทะเบียนเลย</a></button>
            </div>
        </div>
    </div>
</section>