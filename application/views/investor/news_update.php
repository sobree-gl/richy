<!---------------------------------- Content ---------------------------------------->
<section>

    <div class="grid-container display-main">
        <div class="pad-sub-detail">
            <h1 class="font-mint-green" style="margin: 0 0 -5px;"><strong><?php echo lang('NewsUpdate')?></strong></h1>
            <hr>
            <p><strong><a class="a-sub-menu"
                        href="<?=site_url('investor');?>"><?php echo lang('IRMenu')?></a></strong><span
                    class="font-gray-smoke"
                    style="padding: 0 1%;">/</span><span><?php echo lang('NewsActivities')?></a></span><span
                    class="font-gray-smoke" style="padding: 0 1%;">/</span><span class="font-mint-green"><?php echo lang('NewsUpdate')?></span></p>
        </div>



        <div class="table-data">
            <form name="frm1" method="POST" action="news_update.asp">
                <?php
                function DateThai($strDate)
                {
                    $strYear = date("Y",strtotime($strDate))+543;
                    $strMonth= date("n",strtotime($strDate));
                    $strDay= date("j",strtotime($strDate));
                    $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
                    $strMonthThai=$strMonthCut[$strMonth];
                    return "$strDay $strMonthThai $strYear";
                }
    
                function DateEng($strDate)
                {
                    $strYear = date("Y",strtotime($strDate));
                    $strMonth= date("n",strtotime($strDate));
                    $strDay= date("j",strtotime($strDate));
                    $strMonthCut = Array('','Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
                    $strMonthThai=$strMonthCut[$strMonth];
                    return "$strDay $strMonthThai $strYear";
                }

                $news_update = $this->db->get('news_update');
                foreach ($news_update->result_array() as $value) {
                ?>
                <div class="col-lg-12" style="padding-bottom:20px;">
                    <div class="col-lg-2">
                        <img src="<?=base_url();?>images/calendar_icon3.png" width="12">&nbsp;
                        <?php $datee = DateEng($value['date']);
                        if($this->lang->lang()=='th'){ $datee = DateThai($value['date']); } ?>
                        <font class="font_datenews"><?=$datee;?></font>
                    </div>
                    <div class="col-lg-10">
                        <a class="newupdate"
                            href="<?=site_url('investor/read_frame/?topic=news_update&param='.$value['id']);?>"
                            target="_blank"><?=$value['title_'.$this->lang->lang()];?>
                        </a>

                        <img src="<?=base_url();?>images/new_update.gif" border="0">

                    </div>
                </div>
                <?php } ?>


                <!--.................................................. Page Button ..................................................--->
                <!-- <div align="center">
                    <div class="paginator">
                        <div id="paging_top" class="paging" style="padding-top: 20px;font-size: 13px;">

                            <strong>Page: </strong>1 of 7 page(s) &nbsp;&nbsp;&nbsp;
                            <span class="activepage">1</span>

                            <a class="page" href="javascript:SelectedPageChanged('news_update.asp','2');">
                                <font class="font_page">2</font>
                            </a>

                            <a class="page" href="javascript:SelectedPageChanged('news_update.asp','3');">
                                <font class="font_page">3</font>
                            </a>

                            <a class="page" href="javascript:SelectedPageChanged('news_update.asp','4');">
                                <font class="font_page">4</font>
                            </a>

                            <a class="page" href="javascript:SelectedPageChanged('news_update.asp','5');">
                                <font class="font_page">5</font>
                            </a>

                            <a class="page" href="javascript:SelectedPageChanged('news_update.asp','6');">
                                <font class="font_page">6</font>
                            </a>

                            <a class="page" href="javascript:SelectedPageChanged('news_update.asp','7');">
                                <font class="font_page">7</font>
                            </a>

                            <a class="page" href="javascript:SelectedPageChanged('news_update.asp','2');">»</a>

                            <input type="hidden" id="totalpage" name="totalpage" value="7">
                            <input type="hidden" id="pageno" name="pageno" value="1">
                            <input type="hidden" id="pageno_first" name="pageno_first" value="1">
                            <input type="hidden" name="date" id="date" value="">
                            <input type="hidden" name="chkFrist" id="chkFrist" value="">
                            <script language="javascript">
                                function SelectedPageChanged(page, selectedPage) {
                                    document.getElementById("pageno").value = selectedPage;
                                    document.forms["frm1"].submit();
                                }

                                function SelectedPageChangedMore(page, selectedPage) {
                                    document.getElementById("pageno_first").value = selectedPage;
                                    document.getElementById("pageno").value = selectedPage;
                                    document.forms["frm1"].submit();
                                }

                                function SelectedPageChangedNext(page, selectedPage) {
                                    document.getElementById("pageno_first").value = selectedPage;
                                    document.getElementById("pageno").value = selectedPage;
                                    document.forms["frm1"].submit();
                                }

                                function SelectedPageChangedPrev(page, selectedPage, constPageFrist) {
                                    document.getElementById("pageno_first").value = constPageFrist;
                                    document.getElementById("pageno").value = selectedPage;
                                    document.forms["frm1"].submit();
                                }
                            </script>
                        </div>
                    </div>
                </div> -->
                <br>

            </form>

        </div>
    </div>
</section>
<!---------------------------------- Content ---------------------------------------->