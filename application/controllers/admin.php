<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
	}

	public function _example_output($output = null)
	{
		$this->load->view('admin',(array)$output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}


	public function customers()
	{
		$crud = new grocery_CRUD();

		$crud->set_table('customers');
		$crud->set_subject('customers');
		$crud->unset_add();
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function voucher()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('voucher');
		$crud->set_subject('voucher');
		$crud->set_field_upload('file','uploads/voucher');

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function chairman()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('chairman');
		$crud->set_subject('chairman');
		$crud->set_field_upload('image','uploads/chairman');

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function profile_info()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('profile_info');
		$crud->set_subject('profile_info');
		$crud->set_field_upload('image','uploads/profile_info');

		$query = $this->db->get('organization');
		if ($query->num_rows()>0){ $crud->unset_add(); }
		///////////// LIMIT HIDDEN ADD /////////////

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function profile()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('profile');
		$crud->set_subject('profile');

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function passion()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('passion');
		$crud->set_subject('passion');
		$crud->set_field_upload('image','uploads/passion');

		$query = $this->db->get('passion');
		if ($query->num_rows()>2){ $crud->unset_add(); }
		///////////// LIMIT HIDDEN ADD /////////////

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function board()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('board');
		$crud->set_subject('board');
		$crud->columns(array('btn_add','title_th'));

		$crud->callback_column('btn_add',array($this,'board_btn_add'));
		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function board_btn_add($value, $row)
	{
		$return = '
		<a href="'.site_url('admin/board_list/'.$row->id).'">
			<button type="button" class="btn btn-success"><i class="el el-plus"></i> info</button>
		</a>';
		return $return;
	}

	public function board_list($id)
	{
		$crud = new grocery_CRUD();
		$crud->set_table('board_list');
		$crud->set_subject('board_list');

		$crud->where('board_id',$id);
		$crud->field_type('board_id', 'hidden', $id);
		$crud->unset_columns('board_id');
		// ------------------- where -----------------//

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function organization()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('organization');
		$crud->set_subject('organization');
		$crud->set_field_upload('image','uploads/organization');

		$query = $this->db->get('organization');
		if ($query->num_rows()>0){ $crud->unset_add(); }
		///////////// LIMIT HIDDEN ADD /////////////

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function governance()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('governance');
		$crud->set_subject('governance');
		$crud->set_field_upload('image','uploads/governance');
		$crud->set_field_upload('file','uploads/governance');

		$query = $this->db->get('governance');
		if ($query->num_rows()>0){ $crud->unset_add(); }
		///////////// LIMIT HIDDEN ADD /////////////

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function corporate()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('corporate');
		$crud->set_subject('corporate');

		$crud->columns(array('btn_add','title_th','title_en'));

		$crud->callback_column('btn_add',array($this,'corporate_btn_add'));

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function corporate_btn_add($value, $row)
	{
		$return = '
		<a href="'.site_url('admin/corporate_list/'.$row->id).'">
			<button type="button" class="btn btn-success"><i class="el el-plus"></i> corporate list</button>
		</a>';
		return $return;
	}

	public function corporate_list($id)
	{
		$crud = new grocery_CRUD();
		$crud->set_table('corporate_list');
		$crud->set_subject('corporate_list');

		$crud->where('corporate_id',$id);
		$crud->field_type('corporate_id', 'hidden', $id);
		$crud->unset_columns('corporate_id');
		// ------------------- where -----------------//

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function major()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('major');
		$crud->set_subject('major');

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function major_info()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('major_info');
		$crud->set_subject('major_info');

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function meeting_category()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('meeting_category');
		$crud->set_subject('meeting_category');
		$crud->columns(array('btn_add','title_th'));

		$crud->callback_column('btn_add',array($this,'meeting_category_btn_add'));
		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function meeting_category_btn_add($value, $row)
	{
		$return = '
		<a href="'.site_url('admin/meeting_subcategory/'.$row->id).'">
			<button type="button" class="btn btn-success"><i class="el el-plus"></i> subcategory</button>
		</a>';
		return $return;
	}

	public function meeting_subcategory($id)
	{
		$crud = new grocery_CRUD();
		$crud->set_table('meeting_subcategory');
		$crud->set_subject('meeting_subcategory');
		$crud->columns(array('btn_add','title_th'));

		$crud->where('cat_id',$id);
		$crud->field_type('cat_id', 'hidden', $id);
		$crud->unset_columns('cat_id');
		// ------------------- where -----------------//

		$crud->callback_column('btn_add',array($this,'meeting_btn_add'));
		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function meeting_btn_add($value, $row)
	{
		$return = '
		<a href="'.site_url('admin/meeting/'.$row->id).'">
			<button type="button" class="btn btn-primary"><i class="el el-plus"></i> meeting</button>
		</a>';
		return $return;
	}

	public function meeting($id)
	{
		$crud = new grocery_CRUD();

		$this->load->config('grocery_crud');
		$this->config->set_item('grocery_crud_file_upload_allow_file_types','pdf');
		
		$crud->set_table('meeting');
		$crud->set_subject('meeting');

		$crud->where('subcat_id',$id);
		$crud->field_type('subcat_id', 'hidden', $id);
		$crud->unset_columns('subcat_id');
		// ------------------- where -----------------//

		$crud->set_field_upload('file','uploads/meeting');
		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function dividend_info()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('dividend_info');
		$crud->set_subject('dividend_info');
		$crud->set_field_upload('image','uploads/dividend_info');

		$output = $crud->render();
		$this->_example_output($output);
	}

	public function dividend()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('dividend');
		$crud->set_subject('dividend');

		$crud->field_type('Type','dropdown',array('Cash Dividend' => 'Cash Dividend', 'Stock Dividend' => 'Stock Dividend'));

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function webcasts()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('webcasts');
		$crud->set_subject('webcasts');
		$crud->set_field_upload('file','uploads/webcasts');
		$crud->set_field_upload('vdo','uploads/webcasts');

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function calendar()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('calendar');
		$crud->set_subject('calendar');
		$crud->set_field_upload('file','uploads/calendar');
		$crud->set_field_upload('vdo','uploads/calendar');

		$output = $crud->render();
		$this->_example_output($output);
	}

	public function news_update()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('news_update');
		$crud->set_subject('news_update');

		$output = $crud->render();
		$this->_example_output($output);
	}

	public function news_clipping()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('news_clipping');
		$crud->set_subject('news_clipping');
		$crud->set_field_upload('file','uploads/news_clipping');
		$crud->set_field_upload('image','uploads/news_clipping');

		$output = $crud->render();
		$this->_example_output($output);
	}

	public function set_announcement()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('set_announcement');
		$crud->set_subject('set_announcement');

		$crud->set_field_upload('file','uploads/set_announcement');
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function publicize()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('publicize');
		$crud->set_subject('publicize');

		$crud->field_type('date', 'hidden', date('Y-m-d H:i:s'));
		$crud->set_field_upload('image','uploads/publicize');
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function stock_price()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('stock_price');
		$crud->set_subject('stock_price');
		$crud->set_field_upload('logo','uploads/stock_price');

		$output = $crud->render();
		$this->_example_output($output);
	}

	public function ir_contact()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('ir_contact');
		$crud->set_subject('ir_contact');

		$crud->set_field_upload('image','uploads/ir_contact');
		$output = $crud->render();
		$this->_example_output($output);
	}

	public function complaints_category()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('complaints_category');
		$crud->set_subject('complaints_category');

		$output = $crud->render();
		$this->_example_output($output);
	}

	public function complaints_save()
	{
		$crud = new grocery_CRUD();
		$crud->set_table('complaints_save');
		$crud->set_subject('complaints_save');

		$crud->set_field_upload('file1','uploads/complaints_save');
		$crud->set_field_upload('file2','uploads/complaints_save');
		$crud->set_field_upload('file3','uploads/complaints_save');
		$output = $crud->render();
		$this->_example_output($output);
	}

}
