<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Investor extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->lang->load('custom');
	}

    private function seo()
	{
		$meta           = "<title>title</title>";
		$meta          .= "<meta name='robots' content='noindex,nofollow'/>";
		$meta          .= "<meta name='keywords' content='keywords,keywords'/>";
		$meta          .= "<meta name='description' content='description'/>";
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="title" />';
        $meta 		   .= '<meta property="og:description" content="description" />';
        // $meta 		   .= '<meta property="og:image" content="'.base_url('image/logo/logo.png').'" />';
		return $meta;
    }

	public function index()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/home',
        );
        $this->load->view('template', $data);
    }

    public function chairman()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/chairman',
        );
        $this->load->view('template', $data);
    }

    public function profile()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/profile',
        );
        $this->load->view('template', $data);
    }

    public function passion()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/passion',
        );
        $this->load->view('template', $data);
    }

    public function board()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/board',
        );
        $this->load->view('template', $data);
    }

    public function organization()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/organization',
        );
        $this->load->view('template', $data);
    }

    public function governance()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/governance',
        );
        $this->load->view('template', $data);
    }

    public function finance_statement()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/finance_statement',
        );
        $this->load->view('template', $data);
    }

    public function financial_highlight()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/financial_highlight',
        );
        $this->load->view('template', $data);
    }

    public function mda()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/mda',
        );
        $this->load->view('template', $data);
    }

    public function annual_report()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/annual_report',
        );
        $this->load->view('template', $data);
    }

    public function finance_56()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/finance_56',
        );
        $this->load->view('template', $data);
    }

    public function corporate()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/corporate',
        );
        $this->load->view('template', $data);
    }

    public function major()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/major',
        );
        $this->load->view('template', $data);
    }

    public function general_meeting()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/general_meeting',
        );
        $this->load->view('template', $data);
    }

    public function dividend()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/dividend',
        );
        $this->load->view('template', $data);
    }

    public function factsheet()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/factsheet',
        );
        $this->load->view('template', $data);
    }

    public function webcasts()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/webcasts',
        );
        $this->load->view('template', $data);
    }

    public function read_detail()
	{
        $this->load->view('investor/read_detail');
    }

    public function read_player()
	{
        $this->load->view('investor/read_player');
    }

    public function read_frame()
	{
        $this->load->view('investor/read_frame');
    }

    public function read_frame_data()
	{
        $this->load->view('investor/read_frame_data');
    }

    public function analyst()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/analyst',
        );
        $this->load->view('template', $data);
    }

    public function ir_calendar()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/ir_calendar',
        );
        $this->load->view('template', $data);
    }

    public function news_update()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/news_update',
        );
        $this->load->view('template', $data);
    }

    public function news_clipping()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/news_clipping',
        );
        $this->load->view('template', $data);
    }

    public function set_announcement()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/set_announcement',
        );
        $this->load->view('template', $data);
    }

    public function publicize()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/publicize',
        );
        $this->load->view('template', $data);
    }

    public function publicize_detail()
	{
        $this->load->view('investor/publicize_detail');
    }

    public function press()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/press',
        );
        $this->load->view('template', $data);
    }

    public function stock_price()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/stock_price',
        );
        $this->load->view('template', $data);
    }

    public function calculator()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/calculator',
        );
        $this->load->view('template', $data);
    }

    public function ir_contact()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/ir_contact',
        );
        $this->load->view('template', $data);
    }

    public function request_alerts()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/request_alerts',
        );
        $this->load->view('template', $data);
    }

    public function request_inquiry()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/request_inquiry',
        );
        $this->load->view('template', $data);
    }

    public function faqs()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/faqs',
        );
        $this->load->view('template', $data);
    }

    public function complaints()
	{
        $data = array(
            'seo'     => $this->seo(),
            'content' => 'investor/complaints',
        );
        $this->load->view('template', $data);
    }

    private function upload_image($path, $file) {
        $config['upload_path'] = $path;
        $config['allowed_types'] = '*';
        $config['max_size'] = '';
        $config['max_width']  = '';
        $config['max_height']  = '';
        $config['encrypt_name']  = TRUE;
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload($file)) {
            $error = array('error' => $this->upload->display_errors());
            return false;
        }else{
            $file_data = $this->upload->data();
            $file_name = $file_data['file_name'];
            return $file_name;
        }
    }

    public function complaints_save()
	{
        $input = $this->input->post();
        $path = './uploads/complaints_save/';

        if (isset($_FILES['file1']['name']) && !empty($_FILES['file1']['name'])) {
			////////////////////// upload //////////////////
			$file_name1 = $this->upload_image($path, 'file1');
			////////////////////// upload //////////////////
        }

        if (isset($_FILES['file2']['name']) && !empty($_FILES['file2']['name'])) {
			////////////////////// upload //////////////////
			$file_name2 = $this->upload_image($path, 'file2');
			////////////////////// upload //////////////////
        }

        if (isset($_FILES['file3']['name']) && !empty($_FILES['file3']['name'])) {
			////////////////////// upload //////////////////
			$file_name3 = $this->upload_image($path, 'file3');
			////////////////////// upload //////////////////
        }
        
        $data = array(
            'catagory' => $input['catagory'],
            'description' => $input['description'],
            'file1' => isset($file_name1) ? $file_name1 : NULL,
            'file2' => isset($file_name2) ? $file_name2 : NULL,
            'file3' => isset($file_name3) ? $file_name3 : NULL,
            'name' => $input['name'],
            'telphone' => $input['telphone'],
            'email' => $input['email'],
            'other' => $input['other'],
            'date' => date('Y-m-d H:i:s'),
        );
        $complaints_save = $this->db->insert('complaints_save', $data);
        // var_dump( $input );
        if ($complaints_save === true){
            redirect(site_url('investor/complaints'), 'refresh');
        }
    }

    ////////////////////////////////////
}